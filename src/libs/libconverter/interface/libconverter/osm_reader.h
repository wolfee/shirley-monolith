#pragma once

#include <filesystem>
#include <limits>
#include <string>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <libtypes/routing/types.h>

struct readosm_way_struct;
struct readosm_node_struct;

namespace shirley::converter {

class OSMReader {
public:
    void Read(const std::filesystem::path& path);

    void Write(const std::string& filename);

private:
    struct Way {
        std::vector<int64_t> mNodes;
        std::string mType;
        int64_t mId { std::numeric_limits<int64_t>::max() };
        uint64_t mGeometry;
        bool mOneWay { false };
        uint16_t mLength { 0 };
        uint16_t mTravelTime { std::numeric_limits<uint16_t>::max() };
        uint8_t mSpeed { 0 };
    };

    class RunningProgress {
    public:
        RunningProgress();
        ~RunningProgress();
        void Start(const std::string& message);
        void Stop();

    private:
        std::thread mThread;
        bool mAlive { false };
        bool mRunning { false };
    };

    class BarProgress {
    public:
        void Start(int maxValue, int displayStep, int _break, int lineBreak, const std::string& message);
        void Step();
        void Stop();

    private:
        int mMaxValue { 0 };
        int mDisplayStep { 0 };
        int mBreak { 0 };
        int mLineBreak { 0 };
        int mValue { 0 };
    };

    static int ReadWay(const void* user_data, const readosm_way_struct* way);
    int ReadWayImpl(const readosm_way_struct* way);

    static int ReadNode(const void* user_data, const readosm_node_struct* node);
    int ReadNodeImpl(const readosm_node_struct* node);

    static const std::unordered_set<std::string> sUsableHighways;

    std::vector<Way> mWays;
    std::vector<shirley::types::routing::Junction> mJunctions;
    std::vector<shirley::types::routing::Edge> mEdges;
    std::unordered_map<int64_t, shirley::types::routing::GCoor> mNodeMap;
    std::vector<std::vector<shirley::types::routing::GCoor>> mGeometries;
    std::vector<std::vector<uint32_t>> mGeometryEdgeListMap;
};

} // namespace shirley::converter
