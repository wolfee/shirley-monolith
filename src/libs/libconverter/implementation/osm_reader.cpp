#include "../interface/libconverter/osm_reader.h"

#include <algorithm>
#include <cmath>
#include <cstring>
#include <fstream>
#include <ios>
#include <iostream>
#include <libutils/io.h>
#include <limits>
#include <ostream>
#include <polylineencoder.h>
#include <readosm.h>
#include <stdexcept>
#include <string>
#include <thread>
#include <unordered_map>

using namespace std::string_literals;
using namespace std::chrono_literals;

namespace shirley::converter {

const std::unordered_set<std::string> OSMReader::sUsableHighways { "motorway", "trunk", "primary", "secondary", "tertiary", "motorway_link",
    "trunk_link", "primary_link", "secondary_link", "tertiary_link", "unclassified", "residential", "living_street", "service", "track",
    "road" };

OSMReader::RunningProgress::RunningProgress()
{
    mAlive = true;
    mThread = std::thread([&alive = this->mAlive, &running = this->mRunning]() {
        while (alive) {
            int i = 0;
            while (running) {
                if (i != 0 && i % 5 == 0) {
                    std::cout << '\n';
                }
                i++;
                std::cout << '.';
                std::cout.flush();
                std::this_thread::sleep_for(1s);
            }
            std::this_thread::sleep_for(10ms);
        }
    });
}

OSMReader::RunningProgress::~RunningProgress()
{
    mRunning = false;
    mAlive = false;
    mThread.join();
}

void OSMReader::RunningProgress::Start(const std::string& message)
{
    std::cout << message << std::endl;
    mRunning = true;
}

void OSMReader::RunningProgress::Stop()
{
    std::cout << "\n✔ Done" << std::endl;
    mRunning = false;
}

void OSMReader::BarProgress::Start(int maxValue, int displayStep, int _break, int lineBreak, const std::string& message)
{
    mMaxValue = maxValue;
    mDisplayStep = displayStep;
    mBreak = _break;
    mLineBreak = lineBreak;
    mValue = 0;
    std::cout << message;
}

void OSMReader::BarProgress::Step()
{
    if (mValue % mLineBreak == 0) {
        std::cout << '\n' << std::setw(static_cast<int>(std::to_string(mMaxValue).size())) << mValue << ' ' << std::setw(1);
    }

    mValue++;

    if (mValue % mDisplayStep == 0) {
        std::cout << '.';
    }
    if (mValue % mBreak == 0) {
        std::cout << ' ';
    }
    if (mValue % mLineBreak == 0) {
        std::cout << static_cast<int>(static_cast<float>(mValue) / static_cast<float>(mMaxValue) * 100.f) << '%';
    }

    std::cout.flush();
}

void OSMReader::BarProgress::Stop()
{
    if (mValue % mLineBreak != 0) {
        while (mValue % mLineBreak != 0) {
            mValue++;
            if (mValue % mDisplayStep == 0) {
                std::cout << ' ';
            }
            if (mValue % mBreak == 0) {
                std::cout << ' ';
            }
        }
    }
    std::cout << "100%\n✔ Done" << std::endl;
}

void OSMReader::Read(const std::filesystem::path& path)
{
    const void* osm_handle { nullptr };

    RunningProgress spinner;
    BarProgress progress;

    spinner.Start("Reading OSM");
    if (readosm_open(path.c_str(), &osm_handle) != READOSM_OK) {
        readosm_close(osm_handle);
        throw(std::runtime_error("unable to open file"));
    }
    if (readosm_parse(osm_handle, this, nullptr, OSMReader::ReadWay, nullptr) != READOSM_OK) {
        readosm_close(osm_handle);
        throw(std::runtime_error("error on reading ways"));
    }
    readosm_close(osm_handle);
    spinner.Stop();

    progress.Start(static_cast<int>(mWays.size()), 1000, 10000, 50000, "Detecting shared nodes");
    std::unordered_set<int64_t> sharedNodes;
    for (const auto& way : mWays) {
        sharedNodes.insert(way.mNodes.front());
        sharedNodes.insert(way.mNodes.back());
        mNodeMap.emplace(way.mNodes.front(), shirley::types::routing::GCoor {});
        mNodeMap.emplace(way.mNodes.back(), shirley::types::routing::GCoor {});
        for (const auto& id : way.mNodes) {
            if (mNodeMap.find(id) != mNodeMap.end()) {
                sharedNodes.insert(id);
            } else {
                mNodeMap.emplace(id, shirley::types::routing::GCoor {});
            }
        }
        progress.Step();
    }
    progress.Stop();

    spinner.Start("Reading nodes");
    if (readosm_open(path.c_str(), &osm_handle) != READOSM_OK) {
        readosm_close(osm_handle);
        throw(std::runtime_error("unable to open file"));
    }
    if (readosm_parse(osm_handle, this, OSMReader::ReadNode, nullptr, nullptr) != READOSM_OK) {
        readosm_close(osm_handle);
        throw(std::runtime_error("error on reading nodes"));
    }
    readosm_close(osm_handle);
    spinner.Stop();

    progress.Start(static_cast<int>(mWays.size()), 1000, 10000, 50000, "Splitting ways");
    std::vector<Way> splitWays;
    for (const auto& way : mWays) {
        std::vector<size_t> splitPoints;
        for (size_t i = 0; i < way.mNodes.size(); ++i) {
            if (sharedNodes.find(way.mNodes[i]) != sharedNodes.end()) {
                splitPoints.push_back(i);
            }
        }

        for (size_t i = 1; i < splitPoints.size(); ++i) {
            Way newWay(way);
            newWay.mNodes.clear();
            for (size_t j = splitPoints[i - 1]; j <= splitPoints[i]; ++j) {
                newWay.mNodes.push_back(way.mNodes[j]);
            }
            splitWays.push_back(newWay);
        }
        progress.Step();
    }
    mWays.swap(splitWays);
    splitWays.clear();
    progress.Stop();

    progress.Start(static_cast<int>(mWays.size()), 1000, 10000, 50000, "Populating way data");
    for (auto& way : mWays) {
        std::vector<shirley::types::routing::GCoor> geometry;
        for (const auto& node : way.mNodes) {
            geometry.push_back(mNodeMap.at(node));
            if (geometry.back().mLatitude == shirley::types::routing::GCoor::sInvalidCoor) {
                throw std::runtime_error("invalid position!");
            }
        }
        way.mGeometry = mGeometries.size();
        mGeometries.push_back(geometry);

        float length { 0 };
        for (size_t i = 1; i < geometry.size(); ++i) {
            length += geometry[i - 1].GetDistancePrecise(geometry[i]);
        }
        way.mLength = static_cast<uint16_t>(lroundf(length));

        float tt = std::max(1.F, static_cast<float>(way.mLength) / static_cast<float>(way.mSpeed) * 1000.F / 3600.F * 100.F);
        if (tt > std::numeric_limits<uint16_t>::max()) {
            throw std::runtime_error("travel time is too big!");
        }

        way.mTravelTime = static_cast<uint16_t>(lroundf(tt));
        progress.Step();
    }
    progress.Stop();

    progress.Start(static_cast<int>(sharedNodes.size()), 1000, 10000, 50000, "Creating junctions");
    std::unordered_map<int64_t, uint64_t> junctionMap;
    for (const auto& node : sharedNodes) {
        shirley::types::routing::Junction junction;
        junction.mGCoor = mNodeMap.at(node);
        junctionMap[node] = static_cast<uint32_t>(mJunctions.size());
        mJunctions.push_back(junction);
        if (mJunctions.size() == std::numeric_limits<uint32_t>::max()) {
            throw std::runtime_error("too many junctions!");
        }
        progress.Step();
    }
    progress.Stop();

    progress.Start(static_cast<int>(mWays.size()), 1000, 10000, 50000, "Creating edges");
    std::unordered_map<uint32_t, std::vector<shirley::types::routing::Edge>> junctionOutgoings;
    for (auto& way : mWays) {
        shirley::types::routing::Edge forward;
        const auto fromJunctionId = static_cast<uint32_t>(junctionMap.at(way.mNodes.front()));
        const auto endJunctionId = static_cast<uint32_t>(junctionMap.at(way.mNodes.back()));
        if (way.mGeometry > 0x00FFFFFFU) {
            throw std::runtime_error("too big geometry id!");
        }
        forward.mFlags8mGeometryId24 = static_cast<uint32_t>(way.mGeometry);
        forward.mEndJunctionId = static_cast<uint32_t>(junctionMap.at(way.mNodes.back()));
        forward.mLength = way.mLength;
        forward.mTravelTime = way.mTravelTime;
        if (way.mOneWay) {
            forward.mFlags8mGeometryId24 |= (shirley::types::routing::Edge::FLAG_ONEWAY << 24U);
        }
        junctionOutgoings[fromJunctionId].push_back(forward);

        if (!way.mOneWay) {
            shirley::types::routing::Edge backward;
            backward.mFlags8mGeometryId24 = static_cast<uint32_t>(way.mGeometry);
            backward.mEndJunctionId = static_cast<uint32_t>(junctionMap.at(way.mNodes.front()));
            backward.mLength = way.mLength;
            backward.mTravelTime = way.mTravelTime;
            backward.mFlags8mGeometryId24 |= (static_cast<uint32_t>(shirley::types::routing::Edge::FLAG_DIRECTION) << 24U);
            junctionOutgoings[endJunctionId].push_back(backward);
        }
        progress.Step();
    }
    progress.Stop();

    progress.Start(static_cast<int>(junctionOutgoings.size()), 1000, 10000, 50000, "Repacking edges");
    for (const auto& [junctionId, edges] : junctionOutgoings) {
        if (mEdges.size() > 0x00FFFFFFU) {
            throw std::runtime_error("too many edges!");
        }
        if (edges.size() > std::numeric_limits<uint8_t>::max()) {
            throw std::runtime_error("too many outgoings!");
        }
        mJunctions.at(junctionId).mOutgoingCount8mOutgoingStart24 = static_cast<uint32_t>(mEdges.size());
        mJunctions.at(junctionId).mOutgoingCount8mOutgoingStart24 |= (static_cast<uint32_t>(edges.size()) << 24U);
        for (const auto& edge : edges) {
            mEdges.push_back(edge);
        }
        progress.Step();
    }
    progress.Stop();

    progress.Start(static_cast<int>(mEdges.size()), 1000, 10000, 50000, "Collecting geometry to edge index");
    mGeometryEdgeListMap.resize(mGeometries.size());
    for (uint32_t i = 0; i < mEdges.size(); ++i) {
        const auto& element = mEdges[i];
        uint32_t geometryId = element.mFlags8mGeometryId24 & 0x00FFFFFFU;
        mGeometryEdgeListMap[geometryId].push_back(i);
        progress.Step();
    }
    progress.Stop();
}

void OSMReader::Write(const std::string& filename)
{
    RunningProgress spinner;

    spinner.Start("Writing junctions");
    std::ofstream junctions;
    junctions.open(filename + ".junctions", std::ios::out | std::ios::binary);
    const auto junctionCount = static_cast<uint32_t>(mJunctions.size());
    shirley::utils::Write(junctions, junctionCount);
    shirley::utils::WriteMany(junctions, mJunctions);
    junctions.close();
    spinner.Stop();

    spinner.Start("Writing edges");
    std::ofstream edges;
    edges.open(filename + ".edges", std::ios::out | std::ios::binary);
    const auto edgeCount = static_cast<uint32_t>(mEdges.size());
    shirley::utils::Write(edges, edgeCount);
    shirley::utils::WriteMany(edges, mEdges);
    edges.close();
    spinner.Stop();

    spinner.Start("Writing geometries");
    std::ofstream geometries;
    geometries.open(filename + ".geometries", std::ios::out | std::ios::binary);
    const auto geometryCount = static_cast<uint32_t>(mGeometries.size());
    shirley::utils::Write(geometries, geometryCount);
    for (const auto& geometry : mGeometries) {
        gepaf::PolylineEncoder encoder;
        for (const auto& element : geometry) {
            encoder.addPoint(static_cast<double>(element.mLatitude), static_cast<double>(element.mLongitude));
        }
        const auto encoded = encoder.encode();
        if (encoded.size() > std::numeric_limits<uint16_t>::max()) {
            throw std::runtime_error("too long geometry!");
        }
        const auto encodedLength = static_cast<uint16_t>(encoded.size());
        shirley::utils::Write(geometries, encodedLength);
        shirley::utils::WriteMany(geometries, encoded);
    }
    geometries.close();
    spinner.Stop();

    spinner.Start("Writing geometry to edges");
    std::ofstream geometriesToEdge;
    geometriesToEdge.open(filename + ".geometries2edge", std::ios::out | std::ios::binary);
    const auto geometryEdgeCount = static_cast<uint32_t>(mGeometryEdgeListMap.size());
    shirley::utils::Write(geometriesToEdge, geometryEdgeCount);
    for (const auto& edgeList : mGeometryEdgeListMap) {
        if (edgeList.size() > std::numeric_limits<uint8_t>::max()) {
            throw std::runtime_error("too many edges for geometry");
        }
        const auto length = static_cast<uint8_t>(edgeList.size());
        shirley::utils::Write(geometriesToEdge, length);
        shirley::utils::WriteMany(geometriesToEdge, edgeList);
    }
    spinner.Stop();
}

int OSMReader::ReadWay(const void* user_data, const readosm_way* way)
{
    return const_cast<OSMReader*>(static_cast<const OSMReader*>(user_data))->ReadWayImpl(way);
}

int OSMReader::ReadWayImpl(const readosm_way* way)
{
    enum class OneWay { No, Forward, Backward };

    OneWay oneWay { OneWay::No };
    Way newWay;

    for (int i = 0; i < way->tag_count; i++) {
        const auto tag = way->tags[i];

        if (tag.key == "highway"s) {
            if (sUsableHighways.find(tag.value) != sUsableHighways.end()) {
                newWay.mType = tag.value;
            }
        } else if (tag.key == "oneway"s) {
            newWay.mOneWay = true;
            if (tag.value == "yes"s) {
                oneWay = OneWay::Forward;
            } else if (tag.value == "-1"s) {
                oneWay = OneWay::Backward;
            }
        } else if (tag.key == "junction"s && tag.value == "roundabout"s) {
            newWay.mOneWay = true;
        } else if (tag.key == "maxspeed"s) {
            newWay.mSpeed = static_cast<uint8_t>(std::stoi(tag.value));
        }
    }

    if (newWay.mType.empty()) {
        return READOSM_OK;
    }

    for (int i = 0; i < way->node_ref_count; i++) {
        newWay.mNodes.push_back(way->node_refs[i]);
    }

    if (oneWay == OneWay::Backward) {
        std::reverse(newWay.mNodes.begin(), newWay.mNodes.end());
    }

    if (newWay.mSpeed <= 0) {
        newWay.mSpeed = 30;
    }

    newWay.mId = way->id;

    mWays.push_back(newWay);

    return READOSM_OK;
}

int OSMReader::ReadNode(const void* user_data, const readosm_node* way)
{
    return const_cast<OSMReader*>(static_cast<const OSMReader*>(user_data))->ReadNodeImpl(way);
}

int OSMReader::ReadNodeImpl(const readosm_node* node)
{
    if (auto it = mNodeMap.find(node->id); it != mNodeMap.end()) {
        shirley::types::routing::GCoor gcoor;
        gcoor.mLatitude = static_cast<float>(node->latitude);
        gcoor.mLongitude = static_cast<float>(node->longitude);
        it->second = gcoor;
    }

    return READOSM_OK;
}

} // namespace shirley::converter
