const addon = require("./addon.node");

module.exports.RoutePlannerData = addon.RoutePlannerData;
module.exports.RoutePlanner = addon.RoutePlanner;
