#include "librouting/iroute_planner_data.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wshadow"
#include <napi.h>
#pragma GCC diagnostic pop

#include "iroute_planner_data_wrapper.h"
#include "iroute_planner_wrapper.h"

namespace shirley::routing::wrapper {

Napi::Object InitAll(Napi::Env env, Napi::Object exports)
{
    IRoutePlannerDataWrapper::Init(env, exports);
    IRoutePlannerWrapper::Init(env, exports);
    return exports;
}

NODE_API_MODULE(NODE_GYP_MODULE_NAME, InitAll)

} // namespace shirley::routing::wrapper
