#include "iroute_planner_data_wrapper.h"

#include <libutils/io.h>

#include <filesystem>
#include <fstream>

namespace shirley::routing::wrapper {

Napi::FunctionReference IRoutePlannerDataWrapper::constructor;

Napi::Object IRoutePlannerDataWrapper::Init(Napi::Env env, Napi::Object exports)
{
    Napi::Function func = DefineClass(env, "RoutePlannerData", { InstanceMethod("load", &IRoutePlannerDataWrapper::Load) });

    constructor = Napi::Persistent(func);
    constructor.SuppressDestruct();

    exports.Set("RoutePlannerData", func);
    return exports;
}

IRoutePlannerDataWrapper::IRoutePlannerDataWrapper(const Napi::CallbackInfo& info)
    : Napi::ObjectWrap<IRoutePlannerDataWrapper>(info)
{
    if (info.Length() != 0) {
        Napi::TypeError::New(info.Env(), "No parameters expected!").ThrowAsJavaScriptException();
        return;
    }

    mUnderlyingObject = shirley::routing::IRoutePlannerData::Create();
}

void IRoutePlannerDataWrapper::Load(const Napi::CallbackInfo& info)
{
    if (info.Length() != 1 || !info[0].IsString()) {
        Napi::TypeError::New(info.Env(), "No parameters expected!").ThrowAsJavaScriptException();
        return;
    }

    for (const auto& entry : std::filesystem::directory_iterator(std::filesystem::path(info[0].As<Napi::String>()))) {
        std::ifstream file;
        if (entry.path().extension() == ".junctions") {
            file.open(entry.path(), std::ios::binary);
            uint32_t junctionCount;
            shirley::utils::Read(file, junctionCount);
            std::vector<shirley::types::routing::Junction> junctions(junctionCount);
            shirley::utils::ReadMany(file, junctions);

            mUnderlyingObject->SetJunctions(std::move(junctions));
        } else if (entry.path().extension() == ".edges") {
            file.open(entry.path(), std::ios::binary);
            uint32_t edgesCount;
            shirley::utils::Read(file, edgesCount);
            std::vector<shirley::types::routing::Edge> edges(edgesCount);
            shirley::utils::ReadMany(file, edges);

            mUnderlyingObject->SetEdges(std::move(edges));
        }
    }
}

const shirley::routing::IRoutePlannerData& IRoutePlannerDataWrapper::GetUnderlyingObject() { return *mUnderlyingObject; }

} // namespace shirley::routing::wrapper
