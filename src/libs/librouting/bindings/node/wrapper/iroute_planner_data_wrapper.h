#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wshadow"
#include <napi.h>
#pragma GCC diagnostic pop

#include <librouting/iroute_planner_data.h>

namespace shirley::routing::wrapper {

class IRoutePlannerDataWrapper : public Napi::ObjectWrap<IRoutePlannerDataWrapper> {
public:
    static Napi::Object Init(Napi::Env env, Napi::Object exports);
    explicit IRoutePlannerDataWrapper(const Napi::CallbackInfo& info);

    const shirley::routing::IRoutePlannerData& GetUnderlyingObject();

private:
    void Load(const Napi::CallbackInfo& info);

private:
    static Napi::FunctionReference constructor;
    std::unique_ptr<shirley::routing::IRoutePlannerData> mUnderlyingObject;
};

} // namespace shirley::routing::wrapper