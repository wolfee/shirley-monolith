#include "iroute_planner_wrapper.h"

#include "iroute_planner_data_wrapper.h"

#include <librouting/iroute_planner.h>
#include <librouting/iroute_planner_data.h>
#include <libtypes/routing/types.h>

#include <libutils/io.h>

#include <filesystem>
#include <fstream>
#include <memory>

namespace shirley::routing::wrapper {

Napi::FunctionReference IRoutePlannerWrapper::constructor;

Napi::Object IRoutePlannerWrapper::Init(Napi::Env env, Napi::Object exports)
{
    Napi::Function func = DefineClass(env, "RoutePlanner", { InstanceMethod("planRoute", &IRoutePlannerWrapper::PlanRoute) });

    constructor = Napi::Persistent(func);
    constructor.SuppressDestruct();

    exports.Set("RoutePlanner", func);
    return exports;
}

IRoutePlannerWrapper::IRoutePlannerWrapper(const Napi::CallbackInfo& info)
    : Napi::ObjectWrap<IRoutePlannerWrapper>(info)
{
    if (info.Length() != 1 || !info[0].IsObject()) {
        Napi::TypeError::New(info.Env(), "One object parameter expected!").ThrowAsJavaScriptException();
        return;
    }

    IRoutePlannerDataWrapper* routePlannerDataWrapper = Napi::ObjectWrap<IRoutePlannerDataWrapper>::Unwrap(info[0].ToObject());
    if (routePlannerDataWrapper == nullptr) {
        Napi::TypeError::New(info.Env(), "First parameter must be a RoutePlannerData!").ThrowAsJavaScriptException();
        return;
    }

    auto routePlannerData = IRoutePlannerData::Create();
    routePlannerData->SetEdges(std::vector<shirley::types::routing::Edge>(routePlannerDataWrapper->GetUnderlyingObject().GetEdges()));
    routePlannerData->SetJunctions(
        std::vector<shirley::types::routing::Junction>(routePlannerDataWrapper->GetUnderlyingObject().GetJunctions()));

    mUnderlyingObject = shirley::routing::IRoutePlanner::Create(std::move(routePlannerData));
}

Napi::Value IRoutePlannerWrapper::PlanRoute(const Napi::CallbackInfo& info)
{
    if (info.Length() != 1 || !info[0].IsObject()) {
        Napi::TypeError::New(info.Env(), "Routing parameters expected!").ThrowAsJavaScriptException();
        return Env().Undefined();
    }

    const auto& params = info[0].As<Napi::Object>();
    if (!params.Has("from") || !params.Has("to") || !params.Get("from").IsNumber() || !params.Get("to").IsNumber()) {
        Napi::TypeError::New(info.Env(), "From and to are expected routing parameters!").ThrowAsJavaScriptException();
        return Env().Undefined();
    }

    uint32_t from = params.Get("from").As<Napi::Number>();
    uint32_t to = params.Get("to").As<Napi::Number>();

    auto distFunc = IRoutePlanner::FastApproximation;
    if (params.Has("distFunc")) {
        if (!params.Get("distFunc").IsString()) {
            Napi::TypeError::New(info.Env(), "distFunc must is invalid!").ThrowAsJavaScriptException();
            return Env().Undefined();
        }
        std::string distFuncName = params.Get("distFunc").As<Napi::String>();
        if (distFuncName == "fast") {
            distFunc = IRoutePlanner::FastApproximation;
        } else if (distFuncName == "precise") {
            distFunc = IRoutePlanner::Haversine;
        } else {
            Napi::TypeError::New(info.Env(), "distFunc must is invalid!").ThrowAsJavaScriptException();
            return Env().Undefined();
        }
    }

    const auto route = mUnderlyingObject->PlanRoute(from, to, distFunc);

    Napi::Array retVal = Napi::Array::New(info.Env(), route.size());

    for (uint32_t i = 0; i < route.size(); ++i) {
        const auto& element = route[i];
        Napi::Object obj = Napi::Object::New(info.Env());
        obj.Set("endJunctionId", element.mEndJunctionId);
        obj.Set("oneway", static_cast<bool>(element.GetFlags() & types::routing::Edge::Flags::FLAG_ONEWAY));
        obj.Set("direction", static_cast<bool>(element.GetFlags() & types::routing::Edge::Flags::FLAG_DIRECTION) ? "backward" : "forward");
        obj.Set("length", element.mLength);
        obj.Set("travelTime", element.mTravelTime);
        obj.Set("geometryId", element.GetGeometryId());
        retVal[i] = obj;
    }

    return retVal;
}

} // namespace shirley::routing::wrapper
