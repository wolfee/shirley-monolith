#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wshadow"
#include <napi.h>
#pragma GCC diagnostic pop

#include <librouting/iroute_planner.h>

namespace shirley::routing::wrapper {

class IRoutePlannerWrapper : public Napi::ObjectWrap<IRoutePlannerWrapper> {
public:
    static Napi::Object Init(Napi::Env env, Napi::Object exports);
    explicit IRoutePlannerWrapper(const Napi::CallbackInfo& info);

private:
    Napi::Value PlanRoute(const Napi::CallbackInfo& info);

private:
    static Napi::FunctionReference constructor;
    std::unique_ptr<shirley::routing::IRoutePlanner> mUnderlyingObject;
};

} // namespace shirley::routing::wrapper