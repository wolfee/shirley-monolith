#include <gtest/gtest.h>

#include <cinttypes>
#include <librouting/iroute_planner.h>

class RoutePlanningTest : public testing::Test {
public:
    std::unique_ptr<shirley::routing::IRoutePlanner> CreateRoutePlanner(
        const std::vector<std::vector<shirley::types::routing::Edge>> edgesByJunctions)
    {
        std::vector<shirley::types::routing::Edge> finalEdges;
        std::vector<shirley::types::routing::Junction> finalJunctions;
        for (const auto& edgesList : edgesByJunctions) {
            shirley::types::routing::Junction junction;
            junction.mOutgoingCount8mOutgoingStart24 = static_cast<uint32_t>(finalEdges.size());
            junction.mOutgoingCount8mOutgoingStart24 |= static_cast<uint32_t>(edgesList.size() << 24);
            finalJunctions.push_back(junction);
            finalEdges.insert(finalEdges.end(), edgesList.begin(), edgesList.end());
        }

        auto data = shirley::routing::IRoutePlannerData::Create();
        data->SetEdges(std::move(finalEdges));
        data->SetJunctions(std::move(finalJunctions));

        return shirley::routing::IRoutePlanner::Create(std::move(data));
    }
};

// @startuml
// circle 0
// circle 1

// 0-->1: 1
// @enduml
TEST_F(RoutePlanningTest, SingleEdge)
{
    std::vector<std::vector<shirley::types::routing::Edge>> edges {
        { shirley::types::routing::Edge { 1, 0, 1, 1 } },
    };

    const auto rp = CreateRoutePlanner(edges);

    const auto result = rp->PlanRoute(0, 0, shirley::routing::IRoutePlanner::FastApproximation);

    EXPECT_EQ(result.size(), 1);
}

// @startuml
// circle 0
// circle 1
// circle 2

// 0-->1: 1
// 1-->2: 1
// @enduml
TEST_F(RoutePlanningTest, DoubleEdge)
{
    std::vector<std::vector<shirley::types::routing::Edge>> edges {
        { shirley::types::routing::Edge { 1, 0, 1, 1 } },
        { shirley::types::routing::Edge { 2, 0, 1, 1 } },
    };

    const auto rp = CreateRoutePlanner(edges);

    const auto result = rp->PlanRoute(0, 1, shirley::routing::IRoutePlanner::FastApproximation);

    EXPECT_EQ(result.size(), 2);
}

// @startuml
// circle 0
// circle 1
// circle 2
// circle 3

// 0-->1: 1
// 1-->2: 3
// 1-->2: 1
// 2-->3: 1
// @enduml
TEST_F(RoutePlanningTest, Parallels1)
{
    std::vector<std::vector<shirley::types::routing::Edge>> edges {
        {
            shirley::types::routing::Edge { 1, 0, 1, 1 },
        },
        {
            shirley::types::routing::Edge { 2, 0, 1, 1 },
            shirley::types::routing::Edge { 2, 0, 3, 3 },
        },
        {
            shirley::types::routing::Edge { 3, 0, 1, 1 },
        },
    };

    const auto rp = CreateRoutePlanner(edges);

    const auto result = rp->PlanRoute(0, 3, shirley::routing::IRoutePlanner::FastApproximation);

    EXPECT_EQ(result.size(), 3);
    EXPECT_EQ(result.at(0).mTravelTime, 1);
    EXPECT_EQ(result.at(1).mTravelTime, 1);
    EXPECT_EQ(result.at(2).mTravelTime, 1);
}

// @startuml
// circle 0
// circle 1
// circle 2
// circle 3
// circle 4

// 0-->1: 1
// 1-->2: 3
// 1-->3: 1
// 2-->4: 1
// 3-->2: 1
// @enduml
TEST_F(RoutePlanningTest, Parallels2)
{
    std::vector<std::vector<shirley::types::routing::Edge>> edges {
        {
            shirley::types::routing::Edge { 1, 0, 1, 1 },
        },
        {
            shirley::types::routing::Edge { 2, 0, 3, 3 },
            shirley::types::routing::Edge { 3, 0, 1, 1 },
        },
        {
            shirley::types::routing::Edge { 4, 0, 1, 1 },
        },
        {
            shirley::types::routing::Edge { 2, 0, 1, 1 },
        },
    };

    const auto rp = CreateRoutePlanner(edges);

    const auto result = rp->PlanRoute(0, 3, shirley::routing::IRoutePlanner::FastApproximation);

    EXPECT_EQ(result.size(), 4);
}