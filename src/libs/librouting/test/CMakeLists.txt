setup_conan(FILE conanfile.txt)

add_executable(gtest_librouting
    main.cpp
    gtest_route_planner.cpp
)

target_link_libraries(gtest_librouting
    PUBLIC project_options
    PRIVATE project_warnings
    PRIVATE librouting
    PRIVATE CONAN_PKG::gtest
)

include(GoogleTest)
gtest_discover_tests(gtest_librouting)

add_test(NAME gtest_librouting COMMAND gtest_librouting)