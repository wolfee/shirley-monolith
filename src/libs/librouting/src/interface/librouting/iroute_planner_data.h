#pragma once

#include <libtypes/routing/types.h>

#include <memory>
#include <vector>

namespace shirley::routing {

class IRoutePlannerData {
public:
    virtual ~IRoutePlannerData() = default;

    virtual void SetEdges(std::vector<shirley::types::routing::Edge>&& edges) = 0;
    virtual void SetJunctions(std::vector<shirley::types::routing::Junction>&& junctions) = 0;

    virtual const std::vector<shirley::types::routing::Edge>& GetEdges() const = 0;
    virtual const std::vector<shirley::types::routing::Junction>& GetJunctions() const = 0;

    static std::unique_ptr<IRoutePlannerData> Create();
};

} // namespace shirley::routing