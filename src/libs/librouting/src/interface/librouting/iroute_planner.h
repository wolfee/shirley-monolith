#pragma once

#include <libtypes/routing/types.h>

#include <functional>
#include <memory>
#include <vector>

#include "iroute_planner_data.h"

namespace shirley::routing {

class IRoutePlanner {
public:
    using DistanceFunction = std::function<float(const shirley::types::routing::Junction&, const shirley::types::routing::Junction&)>;
    static const DistanceFunction Haversine;
    static const DistanceFunction FastApproximation;

    virtual ~IRoutePlanner() = default;

    virtual std::vector<shirley::types::routing::Edge> PlanRoute(
        uint32_t from, uint32_t to, const DistanceFunction& distanceFunction) const = 0;

    static std::unique_ptr<IRoutePlanner> Create(std::unique_ptr<IRoutePlannerData>&& data);
};

} // namespace shirley::routing
