#pragma once

#include <libtypes/routing/types.h>
#include <memory>
#include <vector>

#include "../interface/librouting/iroute_planner.h"

namespace shirley::routing {

class RoutePlanner : public IRoutePlanner {
public:
    RoutePlanner(std::unique_ptr<IRoutePlannerData>&& data);

    std::vector<shirley::types::routing::Edge> PlanRoute(
        uint32_t from, uint32_t to, const DistanceFunction& distanceFunction) const override;

private:
    std::unique_ptr<IRoutePlannerData> mData;
};

} // namespace shirley::routing
