#include <memory>

#include "route_planner_data.h"

namespace shirley::routing {

void RoutePlannerData::SetEdges(std::vector<shirley::types::routing::Edge>&& edges) { mEdges = std::move(edges); }

void RoutePlannerData::SetJunctions(std::vector<shirley::types::routing::Junction>&& junctions) { mJunctions = std::move(junctions); }

const std::vector<shirley::types::routing::Edge>& RoutePlannerData::GetEdges() const { return mEdges; }

const std::vector<shirley::types::routing::Junction>& RoutePlannerData::GetJunctions() const { return mJunctions; }

std::unique_ptr<IRoutePlannerData> IRoutePlannerData::Create() { return std::make_unique<RoutePlannerData>(); }

} // namespace shirley::routing