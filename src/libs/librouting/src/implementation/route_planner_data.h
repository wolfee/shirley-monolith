#pragma once

#include <vector>

#include "../interface/librouting/iroute_planner_data.h"

namespace shirley::routing {

class RoutePlannerData : public IRoutePlannerData {
public:
    void SetEdges(std::vector<shirley::types::routing::Edge>&& edges) override;
    void SetJunctions(std::vector<shirley::types::routing::Junction>&& junctions) override;

    const std::vector<shirley::types::routing::Edge>& GetEdges() const override;
    const std::vector<shirley::types::routing::Junction>& GetJunctions() const override;

private:
    std::vector<shirley::types::routing::Edge> mEdges;
    std::vector<shirley::types::routing::Junction> mJunctions;
};

} // namespace shirley::routing