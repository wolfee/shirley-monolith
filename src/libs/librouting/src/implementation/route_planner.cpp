#include <algorithm>
#include <functional>
#include <limits>
#include <memory>
#include <queue>
#include <unordered_map>
#include <unordered_set>

#include "route_planner.h"

namespace shirley::routing {

const IRoutePlanner::DistanceFunction IRoutePlanner::Haversine
    = [](const shirley::types::routing::Junction& from, const shirley::types::routing::Junction& to) -> float {
    return from.mGCoor.GetDistancePrecise(to.mGCoor);
};

const IRoutePlanner::DistanceFunction IRoutePlanner::FastApproximation
    = [](const shirley::types::routing::Junction& from, const shirley::types::routing::Junction& to) -> float {
    return from.mGCoor.GetDistanceFast(to.mGCoor);
};

RoutePlanner::RoutePlanner(std::unique_ptr<IRoutePlannerData>&& data)
    : mData { std::move(data) }
{
}

std::vector<shirley::types::routing::Edge> RoutePlanner::PlanRoute(
    uint32_t from, uint32_t to, const DistanceFunction& /* distanceFunction */) const
{
    struct SearchElement {
        uint32_t mId = std::numeric_limits<uint32_t>::max();
        uint32_t mPrevIdx = std::numeric_limits<uint32_t>::max();
        uint32_t mDuration { 0 };

        bool operator<(const SearchElement& other) const { return mDuration < other.mDuration; }
        bool operator>(const SearchElement& other) const { return mDuration > other.mDuration; }
    };

    std::priority_queue<SearchElement, std::vector<SearchElement>, std::greater<>> queue;
    std::vector<SearchElement> visitedEdges;

    const auto& edges = mData->GetEdges();
    const auto& junctions = mData->GetJunctions();

    SearchElement start;
    start.mId = from;
    start.mDuration = edges[from].mTravelTime;
    queue.push(start);
    std::unordered_map<uint32_t, uint32_t> visitedJunctions;

    while (!queue.empty()) {
        SearchElement top = queue.top();
        queue.pop();
        visitedEdges.push_back(top);
        if (top.mId == to) {
            break;
        }

        const shirley::types::routing::Edge& edge = edges[top.mId];
        auto it = visitedJunctions.find(edge.mEndJunctionId);
        if (it != visitedJunctions.end()) {
            if (it->second > top.mDuration) {
                it->second = top.mDuration;
            } else {
                continue;
            }
        } else {
            visitedJunctions.emplace(edge.mEndJunctionId, top.mDuration);
        }

        const shirley::types::routing::Junction& endJunction = junctions[edge.mEndJunctionId];

        const uint32_t outgoingStart = (endJunction.mOutgoingCount8mOutgoingStart24) & 0x00FFFFFFU;
        const uint8_t outgoingCount = static_cast<uint8_t>((endJunction.mOutgoingCount8mOutgoingStart24) >> 24U);

        for (uint8_t i = 0; i < outgoingCount; ++i) {
            SearchElement newElement;
            const shirley::types::routing::Edge& outEdge = edges[outgoingStart + i];
            newElement.mId = outgoingStart + i;
            newElement.mPrevIdx = static_cast<uint32_t>(visitedEdges.size()) - 1;
            newElement.mDuration = top.mDuration + outEdge.mTravelTime;
            queue.push(newElement);
        }
    }

    if (visitedEdges.back().mId != to) {
        return {};
    }

    std::vector<shirley::types::routing::Edge> retVal;
    SearchElement* currentElement = &visitedEdges.back();
    while (currentElement->mId != from) {
        retVal.push_back(edges[currentElement->mId]);
        currentElement = &visitedEdges[currentElement->mPrevIdx];
    }
    retVal.push_back(edges[currentElement->mId]);

    std::reverse(retVal.begin(), retVal.end());

    return retVal;
}

std::unique_ptr<IRoutePlanner> IRoutePlanner::Create(std::unique_ptr<IRoutePlannerData>&& data)
{
    return std::make_unique<RoutePlanner>(std::move(data));
}

} // namespace shirley::routing