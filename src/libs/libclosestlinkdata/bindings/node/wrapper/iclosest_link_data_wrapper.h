#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wshadow"
#include <napi.h>
#pragma GCC diagnostic pop

#include <libclosestlinkdata/iclosest_link_data.h>

namespace shirley::closest_link_data::wrapper {

class IClosestLinkDataWrapper : public Napi::ObjectWrap<IClosestLinkDataWrapper> {
public:
    static Napi::Object Init(Napi::Env env, Napi::Object exports);
    explicit IClosestLinkDataWrapper(const Napi::CallbackInfo& info);

private:
    void Load(const Napi::CallbackInfo& info);
    Napi::Value GetCount(const Napi::CallbackInfo& info);
    Napi::Value Get(const Napi::CallbackInfo& info);

private:
    static Napi::FunctionReference constructor;
    std::unique_ptr<shirley::closest_link_data::IClosestLinkData> mUnderlyingObject;
};

} // namespace shirley::closest_link_data::wrapper
