#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wshadow"
#include <napi.h>
#pragma GCC diagnostic pop

#include "iclosest_link_data_wrapper.h"

namespace shirley::closest_link_data::wrapper {

Napi::Object InitAll(Napi::Env env, Napi::Object exports)
{
    IClosestLinkDataWrapper::Init(env, exports);
    return exports;
}

NODE_API_MODULE(NODE_GYP_MODULE_NAME, InitAll)

} // namespace shirley::closest_link_data::wrapper
