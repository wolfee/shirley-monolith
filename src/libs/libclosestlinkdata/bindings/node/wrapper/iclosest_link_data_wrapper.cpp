#include "iclosest_link_data_wrapper.h"
#include "libclosestlinkdata/iclosest_link_data.h"
#include "napi.h"

#include <cstddef>
#include <libutils/io.h>

#include <filesystem>
#include <fstream>

namespace shirley::closest_link_data::wrapper {

Napi::FunctionReference IClosestLinkDataWrapper::constructor;

Napi::Object IClosestLinkDataWrapper::Init(Napi::Env env, Napi::Object exports)
{
    Napi::Function func = DefineClass(env, "ClosestLinkData",
        { InstanceMethod("load", &IClosestLinkDataWrapper::Load), InstanceMethod("getCount", &IClosestLinkDataWrapper::GetCount),
            InstanceMethod("get", &IClosestLinkDataWrapper::Get) });

    constructor = Napi::Persistent(func);
    constructor.SuppressDestruct();

    exports.Set("ClosestLinkData", func);
    return exports;
}

IClosestLinkDataWrapper::IClosestLinkDataWrapper(const Napi::CallbackInfo& info)
    : Napi::ObjectWrap<IClosestLinkDataWrapper>(info)
{
    if (info.Length() != 0) {
        Napi::TypeError::New(info.Env(), "No parameters expected!").ThrowAsJavaScriptException();
        return;
    }

    mUnderlyingObject = shirley::closest_link_data::IClosestLinkData::Create();
}

void IClosestLinkDataWrapper::Load(const Napi::CallbackInfo& info)
{
    if (info.Length() != 1 || !info[0].IsString()) {
        Napi::TypeError::New(info.Env(), "Path expected!").ThrowAsJavaScriptException();
        return;
    }

    for (const auto& entry : std::filesystem::directory_iterator(std::filesystem::path(info[0].As<Napi::String>()))) {
        std::ifstream file;
        if (entry.path().extension() == ".geometries") {
            file.open(entry.path(), std::ios::binary);
            uint32_t geometriesCount;
            shirley::utils::Read(file, geometriesCount);
            std::vector<std::string> geometries(geometriesCount);
            for (auto& geometry : geometries) {
                uint16_t geometryLength;
                shirley::utils::Read(file, geometryLength);
                geometry.resize(geometryLength);
                shirley::utils::ReadMany(file, geometry);
            }

            mUnderlyingObject->SetGeometries(std::move(geometries));
        } else if (entry.path().extension() == ".geometries2edge") {
            file.open(entry.path(), std::ios::binary);
            uint32_t mappingCount { 0 };
            shirley::utils::Read(file, mappingCount);
            std::vector<std::vector<uint32_t>> mappings(mappingCount);
            for (auto& mapping : mappings) {
                uint8_t mappingLength { 0 };
                shirley::utils::Read(file, mappingLength);
                mapping.resize(mappingLength);
                shirley::utils::ReadMany(file, mapping);
            }

            mUnderlyingObject->SetGeometriesToEdge(std::move(mappings));
        }
    }
}

Napi::Value IClosestLinkDataWrapper::GetCount(const Napi::CallbackInfo& info)
{
    return Napi::Number::New(info.Env(), static_cast<double>(mUnderlyingObject->GetCount()));
}

Napi::Value IClosestLinkDataWrapper::Get(const Napi::CallbackInfo& info)
{
    if (info.Length() != 1 || !info[0].IsNumber()) {
        Napi::TypeError::New(info.Env(), "No parameters expected!").ThrowAsJavaScriptException();
        return Env().Undefined();
    }

    const auto data = mUnderlyingObject->Get(info[0].As<Napi::Number>().Uint32Value());

    Napi::Object retVal = Napi::Object::New(info.Env());

    Napi::Array gcoors = Napi::Array::New(info.Env(), data.gcoors.size());
    for (size_t i = 0; i < data.gcoors.size(); ++i) {
        Napi::Object coord = Napi::Object::New(info.Env());
        coord.Set("longitude", data.gcoors[i].mLongitude);
        coord.Set("latitude", data.gcoors[i].mLatitude);
        gcoors[static_cast<uint32_t>(i)] = coord;
    }
    retVal.Set("gcoors", gcoors);

    Napi::Array ids = Napi::Array::New(info.Env(), data.id.size());
    for (size_t i = 0; i < data.id.size(); ++i) {
        ids[static_cast<uint32_t>(i)] = data.id[i];
    }
    retVal.Set("ids", ids);

    retVal.Set("valid", data.valid);

    return retVal;
}

} // namespace shirley::closest_link_data::wrapper
