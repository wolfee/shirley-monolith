#pragma once

#include <memory>
#include <string>
#include <vector>

#include <libtypes/routing/types.h>

namespace shirley::closest_link_data {

class IClosestLinkData {
public:
    struct LinkData {
        std::vector<shirley::types::routing::GCoor> gcoors;
        std::vector<uint32_t> id;
        bool valid { false };
    };

    virtual ~IClosestLinkData() = default;

    virtual void SetGeometries(std::vector<std::string>&& geometries) = 0;
    virtual const std::vector<std::string>& GetGeometries() const = 0;

    virtual void SetGeometriesToEdge(std::vector<std::vector<uint32_t>>&& geometriesToEdge) = 0;
    virtual const std::vector<std::vector<uint32_t>>& GetGeometriesToEdge() const = 0;

    virtual size_t GetCount() const = 0;

    virtual LinkData Get(size_t i) const = 0;

    static std::unique_ptr<IClosestLinkData> Create();
};

} // namespace shirley::closest_link_data
