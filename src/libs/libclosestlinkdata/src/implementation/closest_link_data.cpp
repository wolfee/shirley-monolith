#include <memory>
#include <stdexcept>

#include "closest_link_data.h"
#include "libtypes/routing/types.h"

#include <polylineencoder.h>

namespace shirley::closest_link_data {

void ClosestLinkData::SetGeometries(std::vector<std::string>&& geometries) { mGeometries = std::move(geometries); }

const std::vector<std::string>& ClosestLinkData::GetGeometries() const { return mGeometries; }

void ClosestLinkData::SetGeometriesToEdge(std::vector<std::vector<uint32_t>>&& geometriesToEdge)
{
    mGeometriesToEdge = std::move(geometriesToEdge);
}

const std::vector<std::vector<uint32_t>>& ClosestLinkData::GetGeometriesToEdge() const { return mGeometriesToEdge; }

size_t ClosestLinkData::GetCount() const
{
    if (mGeometries.size() != mGeometriesToEdge.size()) {
        throw std::runtime_error("Mismatching element count!");
    }

    return mGeometries.size();
}

ClosestLinkData::LinkData ClosestLinkData::Get(size_t i) const
{
    LinkData retVal;

    const auto& geometry = mGeometries[i];
    const auto coordinates = gepaf::PolylineEncoder::decode(geometry);
    retVal.gcoors.reserve(coordinates.size());

    constexpr double error = 0.000001;

    bool isGoodCandidate = false;

    for (size_t c = 0; c < coordinates.size(); ++c) {
        if (!isGoodCandidate && c < coordinates.size() - 1) {
            if (std::abs(coordinates[c].latitude() - coordinates[c + 1].latitude()) > error
                || std::abs(coordinates[c].longitude() - coordinates[c + 1].longitude()) > error) {
                isGoodCandidate = true;
            }
        }

        shirley::types::routing::GCoor coor;
        coor.mLatitude = static_cast<float>(coordinates[c].latitude());
        coor.mLongitude = static_cast<float>(coordinates[c].longitude());

        retVal.gcoors.push_back(coor);
    }

    retVal.valid = isGoodCandidate;

    retVal.id.reserve(mGeometriesToEdge[i].size());
    for (const auto& id : mGeometriesToEdge[i]) {
        retVal.id.push_back(id);
    }

    return retVal;
}

std::unique_ptr<IClosestLinkData> IClosestLinkData::Create() { return std::make_unique<ClosestLinkData>(); }

} // namespace shirley::closest_link_data