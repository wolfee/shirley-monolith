#pragma once

#include "../interface/libclosestlinkdata/iclosest_link_data.h"

namespace shirley::closest_link_data {

class ClosestLinkData : public IClosestLinkData {
public:
    void SetGeometries(std::vector<std::string>&& geometries) override;
    const std::vector<std::string>& GetGeometries() const override;

    void SetGeometriesToEdge(std::vector<std::vector<uint32_t>>&& geometriesToEdge) override;
    const std::vector<std::vector<uint32_t>>& GetGeometriesToEdge() const override;

    size_t GetCount() const override;

    LinkData Get(size_t i) const override;

private:
    std::vector<std::string> mGeometries;
    std::vector<std::vector<uint32_t>> mGeometriesToEdge;
};

} // namespace shirley::closest_link_data
