#pragma once

#include <libtypes/routing/types.h>
#include <memory>
#include <vector>

#include "../interface/libgeometries/igeometries.h"

namespace shirley::geometries {

class Geometries : public IGeometries {
public:
    explicit Geometries(std::unique_ptr<IGeometriesData>&& data);

    std::vector<std::vector<shirley::types::routing::GCoor>> GetGCoors(std::vector<uint32_t> geometryIds) const override;
    std::vector<std::string> GetPolylines(std::vector<uint32_t> geometryIds) const override;
    std::string GetConcatenatedPolyline(std::vector<uint32_t> geometryIds) const override;

private:
    std::unique_ptr<IGeometriesData> mData;
};

} // namespace shirley::geometries
