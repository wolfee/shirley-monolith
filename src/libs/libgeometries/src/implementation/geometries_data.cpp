#include <memory>

#include "geometries_data.h"

namespace shirley::geometries {

void GeometriesData::SetGeometries(std::vector<std::string>&& geometries) { mGeometries = std::move(geometries); }

const std::vector<std::string>& GeometriesData::GetGeometries() const { return mGeometries; }

std::unique_ptr<IGeometriesData> IGeometriesData::Create() { return std::make_unique<GeometriesData>(); }

} // namespace shirley::geometries