#pragma once

#include "../interface/libgeometries/igeometries_data.h"

namespace shirley::geometries {

class GeometriesData : public IGeometriesData {
public:
    void SetGeometries(std::vector<std::string>&& geometries) override;
    const std::vector<std::string>& GetGeometries() const override;

private:
    std::vector<std::string> mGeometries;
};

} // namespace shirley::geometries