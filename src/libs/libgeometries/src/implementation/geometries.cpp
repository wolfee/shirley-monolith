#include "geometries.h"
#include "libtypes/routing/types.h"

#include <memory>
#include <polylineencoder.h>

namespace shirley::geometries {

Geometries::Geometries(std::unique_ptr<IGeometriesData>&& data)
    : mData(std::move(data))
{
}

std::vector<std::vector<shirley::types::routing::GCoor>> Geometries::GetGCoors(std::vector<uint32_t> geometryIds) const
{
    std::vector<std::vector<shirley::types::routing::GCoor>> retVal;
    retVal.reserve(geometryIds.size());

    for (const auto& element : geometryIds) {
        std::vector<shirley::types::routing::GCoor> finalCoords;
        const auto coords = gepaf::PolylineEncoder::decode(mData->GetGeometries().at(element));
        finalCoords.reserve(coords.size());
        for (const auto& coord : coords) {
            shirley::types::routing::GCoor c;
            c.mLatitude = static_cast<float>(coord.latitude());
            c.mLongitude = static_cast<float>(coord.longitude());
            finalCoords.push_back(c);
        }
        retVal.push_back(finalCoords);
    }

    return retVal;
}

std::vector<std::string> Geometries::GetPolylines(std::vector<uint32_t> geometryIds) const
{
    std::vector<std::string> retVal;
    retVal.reserve(geometryIds.size());

    for (const auto& element : geometryIds) {
        retVal.push_back(mData->GetGeometries().at(element));
    }

    return retVal;
}

std::string Geometries::GetConcatenatedPolyline(std::vector<uint32_t> geometryIds) const
{
    gepaf::PolylineEncoder encoder;
    for (const auto& element : geometryIds) {
        const auto decodedPolyline = gepaf::PolylineEncoder::decode(mData->GetGeometries().at(element));
        for (const auto& point : decodedPolyline) {
            encoder.addPoint(point.latitude(), point.longitude());
        }
    }

    return encoder.encode();
}

std::unique_ptr<IGeometries> IGeometries::Create(std::unique_ptr<IGeometriesData>&& data)
{
    return std::make_unique<Geometries>(std::move(data));
}

} // namespace shirley::geometries