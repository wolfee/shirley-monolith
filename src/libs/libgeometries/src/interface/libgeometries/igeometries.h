#pragma once

#include <memory>
#include <string>
#include <vector>

#include "igeometries_data.h"

#include <libtypes/routing/types.h>

namespace shirley::geometries {

class IGeometries {
public:
    virtual ~IGeometries() = default;

    virtual std::vector<std::vector<shirley::types::routing::GCoor>> GetGCoors(std::vector<uint32_t> geometryIds) const = 0;
    virtual std::vector<std::string> GetPolylines(std::vector<uint32_t> geometryIds) const = 0;
    virtual std::string GetConcatenatedPolyline(std::vector<uint32_t> geometryIds) const = 0;

    static std::unique_ptr<IGeometries> Create(std::unique_ptr<IGeometriesData>&& data);
};

} // namespace shirley::geometries
