#pragma once

#include <memory>
#include <string>
#include <vector>

namespace shirley::geometries {

class IGeometriesData {
public:
    virtual ~IGeometriesData() = default;

    virtual void SetGeometries(std::vector<std::string>&& geometries) = 0;
    virtual const std::vector<std::string>& GetGeometries() const = 0;

    static std::unique_ptr<IGeometriesData> Create();
};

} // namespace shirley::geometries
