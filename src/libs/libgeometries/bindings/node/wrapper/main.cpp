#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wshadow"
#include <napi.h>
#pragma GCC diagnostic pop

#include "igeometries_data_wrapper.h"
#include "igeometries_wrapper.h"

namespace shirley::geometries::wrapper {

Napi::Object InitAll(Napi::Env env, Napi::Object exports)
{
    IGeometriesDataWrapper::Init(env, exports);
    IGeometriesWrapper::Init(env, exports);
    return exports;
}

NODE_API_MODULE(NODE_GYP_MODULE_NAME, InitAll)

} // namespace shirley::geometries::wrapper
