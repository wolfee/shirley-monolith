#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wshadow"
#include <napi.h>
#pragma GCC diagnostic pop

#include <libgeometries/igeometries_data.h>

namespace shirley::geometries::wrapper {

class IGeometriesDataWrapper : public Napi::ObjectWrap<IGeometriesDataWrapper> {
public:
    static Napi::Object Init(Napi::Env env, Napi::Object exports);
    explicit IGeometriesDataWrapper(const Napi::CallbackInfo& info);

    const shirley::geometries::IGeometriesData& GetUnderlyingObject();

private:
    void Load(const Napi::CallbackInfo& info);

private:
    static Napi::FunctionReference constructor;
    std::unique_ptr<shirley::geometries::IGeometriesData> mUnderlyingObject;
};

} // namespace shirley::geometries::wrapper