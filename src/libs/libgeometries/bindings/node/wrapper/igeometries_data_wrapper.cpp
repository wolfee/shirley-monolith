#include "igeometries_data_wrapper.h"

#include <libutils/io.h>

#include <filesystem>
#include <fstream>

namespace shirley::geometries::wrapper {

Napi::FunctionReference IGeometriesDataWrapper::constructor;

Napi::Object IGeometriesDataWrapper::Init(Napi::Env env, Napi::Object exports)
{
    Napi::Function func = DefineClass(env, "GeometriesData", { InstanceMethod("load", &IGeometriesDataWrapper::Load) });

    constructor = Napi::Persistent(func);
    constructor.SuppressDestruct();

    exports.Set("GeometriesData", func);
    return exports;
}

IGeometriesDataWrapper::IGeometriesDataWrapper(const Napi::CallbackInfo& info)
    : Napi::ObjectWrap<IGeometriesDataWrapper>(info)
{
    if (info.Length() != 0) {
        Napi::TypeError::New(info.Env(), "No parameters expected!").ThrowAsJavaScriptException();
        return;
    }

    mUnderlyingObject = shirley::geometries::IGeometriesData::Create();
}

void IGeometriesDataWrapper::Load(const Napi::CallbackInfo& info)
{
    if (info.Length() != 1 || !info[0].IsString()) {
        Napi::TypeError::New(info.Env(), "No parameters expected!").ThrowAsJavaScriptException();
        return;
    }

    for (const auto& entry : std::filesystem::directory_iterator(std::filesystem::path(info[0].As<Napi::String>()))) {
        if (entry.path().extension() == ".geometries") {
            std::ifstream file;
            file.open(entry.path(), std::ios::binary);
            uint32_t geometriesCount;
            shirley::utils::Read(file, geometriesCount);
            std::vector<std::string> geometries(geometriesCount);
            for (auto& geometry : geometries) {
                uint16_t geometryLength;
                shirley::utils::Read(file, geometryLength);
                geometry.resize(geometryLength);
                shirley::utils::ReadMany(file, geometry);
            }

            mUnderlyingObject->SetGeometries(std::move(geometries));
        }
    }
}

const shirley::geometries::IGeometriesData& IGeometriesDataWrapper::GetUnderlyingObject() { return *mUnderlyingObject; }

} // namespace shirley::geometries::wrapper
