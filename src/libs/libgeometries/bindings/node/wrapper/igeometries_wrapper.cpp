#include "igeometries_wrapper.h"

#include "igeometries_data_wrapper.h"
#include "libgeometries/igeometries_data.h"
#include "libtypes/routing/types.h"
#include "napi.h"
#include <vector>

namespace shirley::geometries::wrapper {

Napi::FunctionReference IGeometriesWrapper::constructor;

Napi::Object IGeometriesWrapper::Init(Napi::Env env, Napi::Object exports)
{
    Napi::Function func = DefineClass(env, "Geometries",
        { InstanceMethod("getCoors", &IGeometriesWrapper::GetGCoors), InstanceMethod("getPolylines", &IGeometriesWrapper::GetPolylines),
            InstanceMethod("getConcatenatedPolyline", &IGeometriesWrapper::GetConcatenatedPolyline) });

    constructor = Napi::Persistent(func);
    constructor.SuppressDestruct();

    exports.Set("Geometries", func);
    return exports;
}

IGeometriesWrapper::IGeometriesWrapper(const Napi::CallbackInfo& info)
    : Napi::ObjectWrap<IGeometriesWrapper>(info)
{
    if (info.Length() != 1 || !info[0].IsObject()) {
        Napi::TypeError::New(info.Env(), "One object parameter expected!").ThrowAsJavaScriptException();
        return;
    }

    IGeometriesDataWrapper* geometriesDataWrapper = Napi::ObjectWrap<IGeometriesDataWrapper>::Unwrap(info[0].ToObject());
    if (geometriesDataWrapper == nullptr) {
        Napi::TypeError::New(info.Env(), "First parameter must be a GeometriesData!").ThrowAsJavaScriptException();
        return;
    }

    auto geometriesData = IGeometriesData::Create();
    geometriesData->SetGeometries(std::vector<std::string>(geometriesDataWrapper->GetUnderlyingObject().GetGeometries()));

    mUnderlyingObject = shirley::geometries::IGeometries::Create(std::move(geometriesData));
}

Napi::Value IGeometriesWrapper::GetGCoors(const Napi::CallbackInfo& info)
{
    if (info.Length() != 1 || !info[0].IsArray()) {
        Napi::TypeError::New(info.Env(), "Array of IDs expected!").ThrowAsJavaScriptException();
        return Env().Undefined();
    }

    const auto& ids = info[0].As<Napi::Array>();
    std::vector<uint32_t> realIds;
    realIds.reserve(ids.Length());

    for (uint32_t i = 0; i < ids.Length(); ++i) {
        Napi::Value v = ids[i];
        if (!v.IsNumber()) {
            Napi::TypeError::New(info.Env(), "IDs must be numbers!").ThrowAsJavaScriptException();
            return Env().Undefined();
        }

        realIds.push_back(v.As<Napi::Number>());
    }

    const auto gcoors = mUnderlyingObject->GetGCoors(realIds);

    Napi::Array retVal = Napi::Array::New(info.Env(), gcoors.size());

    for (uint32_t i = 0; i < retVal.Length(); ++i) {
        Napi::Array array = Napi::Array::New(info.Env(), gcoors[i].size());
        for (uint32_t j = 0; j < array.Length(); ++j) {
            Napi::Object object = Napi::Object::New(info.Env());
            object.Set("longitude", gcoors[i][j].mLongitude);
            object.Set("latitude", gcoors[i][j].mLatitude);
            array[j] = object;
        }
        retVal[i] = array;
    }

    return retVal;
}

Napi::Value IGeometriesWrapper::GetPolylines(const Napi::CallbackInfo& info)
{
    if (info.Length() != 1 || !info[0].IsArray()) {
        Napi::TypeError::New(info.Env(), "Array of IDs expected!").ThrowAsJavaScriptException();
        return Env().Undefined();
    }

    const auto& ids = info[0].As<Napi::Array>();
    std::vector<uint32_t> realIds;
    realIds.reserve(ids.Length());

    for (uint32_t i = 0; i < ids.Length(); ++i) {
        Napi::Value v = ids[i];
        if (!v.IsNumber()) {
            Napi::TypeError::New(info.Env(), "IDs must be numbers!").ThrowAsJavaScriptException();
            return Env().Undefined();
        }

        realIds.push_back(v.As<Napi::Number>());
    }

    const auto polylines = mUnderlyingObject->GetPolylines(realIds);

    Napi::Array retVal = Napi::Array::New(info.Env(), polylines.size());

    for (uint32_t i = 0; i < retVal.Length(); ++i) {
        retVal[i] = Napi::String::New(info.Env(), polylines[i]);
    }

    return retVal;
}

Napi::Value IGeometriesWrapper::GetConcatenatedPolyline(const Napi::CallbackInfo& info)
{
    if (info.Length() != 1 || !info[0].IsArray()) {
        Napi::TypeError::New(info.Env(), "Array of IDs expected!").ThrowAsJavaScriptException();
        return Env().Undefined();
    }

    const auto& ids = info[0].As<Napi::Array>();
    std::vector<uint32_t> realIds;
    realIds.reserve(ids.Length());

    for (uint32_t i = 0; i < ids.Length(); ++i) {
        Napi::Value v = ids[i];
        if (!v.IsNumber()) {
            Napi::TypeError::New(info.Env(), "IDs must be numbers!").ThrowAsJavaScriptException();
            return Env().Undefined();
        }

        realIds.push_back(v.As<Napi::Number>());
    }

    return Napi::String::New(info.Env(), mUnderlyingObject->GetConcatenatedPolyline(realIds));
}

} // namespace shirley::geometries::wrapper
