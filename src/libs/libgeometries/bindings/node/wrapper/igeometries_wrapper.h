#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wshadow"
#include <napi.h>
#pragma GCC diagnostic pop

#include <libgeometries/igeometries.h>

namespace shirley::geometries::wrapper {

class IGeometriesWrapper : public Napi::ObjectWrap<IGeometriesWrapper> {
public:
    static Napi::Object Init(Napi::Env env, Napi::Object exports);
    explicit IGeometriesWrapper(const Napi::CallbackInfo& info);

private:
    Napi::Value GetGCoors(const Napi::CallbackInfo& info);
    Napi::Value GetPolylines(const Napi::CallbackInfo& info);
    Napi::Value GetConcatenatedPolyline(const Napi::CallbackInfo& info);

private:
    static Napi::FunctionReference constructor;
    std::unique_ptr<shirley::geometries::IGeometries> mUnderlyingObject;
};

} // namespace shirley::geometries::wrapper