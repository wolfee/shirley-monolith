#pragma once

#include <istream>
#include <ostream>

namespace shirley::utils {

template <typename T>
void Read(std::istream& from, T& to)
{
    from.read(reinterpret_cast<char*>(&to), sizeof(to));
}

template <typename T>
void ReadMany(std::istream& from, T& to)
{
    from.read(reinterpret_cast<char*>(to.data()), static_cast<uint32_t>(to.size() * sizeof(to[0])));
}

template <typename T>
void Write(std::ostream& to, const T& from)
{
    to.write(reinterpret_cast<const char*>(&from), sizeof(from));
}

template <typename T>
void WriteMany(std::ostream& to, const T& from)
{
    to.write(reinterpret_cast<const char*>(from.data()), static_cast<uint32_t>(from.size() * sizeof(from[0])));
}

} // namespace shirley::utils