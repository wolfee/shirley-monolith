#pragma once

#include <string>

namespace shirley::utils {

class IProgressbar {
public:
    virtual ~IProgressbar() = default;
    virtual void Start(const std::string& message, size_t maxValue, size_t resolution = 1) = 0;
    virtual void SetValue(size_t value) = 0;
    virtual void Tick() = 0;
    virtual void Stop(const std::string& message = "Done!") = 0;

    static IProgressbar& Get();
};

} // namespace shirley::utils