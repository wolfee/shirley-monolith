#pragma once

#include <string>

namespace shirley::utils {

class ISpinner {
public:
    virtual ~ISpinner() = default;

    virtual void Start(const std::string& message) = 0;
    virtual void Stop(const std::string& message = "Done!") = 0;

    static ISpinner& Get();
};

} // namespace shirley::utils