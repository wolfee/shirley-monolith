#include "progressbar.h"
#include <indicators/block_progress_bar.hpp>
#include <indicators/cursor_control.hpp>

namespace shirley::utils {

void Progressbar::Start(const std::string& message, size_t maxValue, size_t resolution)
{
    if (mProgressbar) {
        throw std::runtime_error("A progressbar is already running!");
    }

    std::cout << termcolor::bold << termcolor::yellow << message << std::endl;
    std::cout << termcolor::reset;

    mMaxValue = maxValue;
    mValue = 0;
    mResolution = resolution;

    mProgressbar.reset(std::make_unique<indicators::BlockProgressBar>(indicators::option::BarWidth { 40 },
        indicators::option::ForegroundColor { indicators::Color::yellow },
        indicators::option::FontStyles { std::vector<indicators::FontStyle> { indicators::FontStyle::bold } },
        indicators::option::MaxProgress { mMaxValue }, indicators::option::ShowElapsedTime { true },
        indicators::option::ShowRemainingTime { true })
                           .release());

    mProgressbar->set_option(indicators::option::PostfixText { std::to_string(0) + "/" + std::to_string(mMaxValue) });
    mProgressbar->set_progress(0);

    indicators::show_console_cursor(false);
}

void Progressbar::SetValue(size_t value)
{
    if (!mProgressbar) {
        throw std::runtime_error("No progressbar running!");
    }

    mValue = value;

    mProgressbar->set_progress(static_cast<float>(mValue));

    mProgressbar->set_option(
        indicators::option::PostfixText { std::to_string(std::min(mValue, mMaxValue)) + "/" + std::to_string(mMaxValue) + "\n" });
    std::cout.flush();
}

void Progressbar::Tick()
{
    mValue++;
    if (mValue % mResolution == 0) {
        SetValue(mValue);
    }
}

void Progressbar::Stop(const std::string& message)
{
    if (!mProgressbar) {
        throw std::runtime_error("No progressbar running!");
    }

    mValue = mMaxValue;
    SetValue(mValue);

    mProgressbar->mark_as_completed();

    std::cout << termcolor::bold << termcolor::green << "✔ " << message << std::endl;
    std::cout << termcolor::reset;

    mProgressbar.reset(nullptr);
    indicators::show_console_cursor(true);
}

IProgressbar& IProgressbar::Get()
{
    static Progressbar bar;
    return bar;
}

} // namespace shirley::utils