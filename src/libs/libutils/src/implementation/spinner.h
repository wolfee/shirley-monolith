#pragma once

#include "../interface/libutils/ispinner.h"

#include <indicators/progress_spinner.hpp>
#include <thread>

namespace shirley::utils {

class Spinner : public ISpinner {
public:
    ~Spinner() override;
    void Start(const std::string& message) override;
    void Stop(const std::string& message) override;

private:
    std::thread mWorker;
    std::unique_ptr<indicators::ProgressSpinner> mSpinner;
    bool mRunning { false };
};

} // namespace shirley::utils