#pragma once

#include "../interface/libutils/iprogress_bar.h"

#include <indicators/block_progress_bar.hpp>

namespace shirley::utils {

class Progressbar : public IProgressbar {
public:
    void Start(const std::string& message, size_t maxValue, size_t resolutuion) override;
    void SetValue(size_t value) override;
    void Tick() override;
    void Stop(const std::string& message) override;

private:
    std::unique_ptr<indicators::BlockProgressBar> mProgressbar;
    size_t mMaxValue { 0 };
    size_t mValue { 0 };
    size_t mResolution { 1 };
};

} // namespace shirley::utils