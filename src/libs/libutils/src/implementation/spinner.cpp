#include "spinner.h"
#include <indicators/cursor_control.hpp>
#include <memory>
#include <stdexcept>

namespace shirley::utils {

Spinner::~Spinner()
{
    mRunning = false;
    if (mWorker.joinable()) {
        mWorker.join();
    }
}

void Spinner::Start(const std::string& message)
{
    if (mRunning) {
        throw std::runtime_error("A spinner is already running!");
    }

    mSpinner.reset(std::make_unique<indicators::ProgressSpinner>(indicators::option::PostfixText { message },
        indicators::option::ForegroundColor { indicators::Color::yellow },
        indicators::option::SpinnerStates { std::vector<std::string> { "|", "/", "-", "\\" } },
        indicators::option::FontStyles { std::vector<indicators::FontStyle> { indicators::FontStyle::bold } },
        indicators::option::ShowPercentage { false })
                       .release());

    mRunning = true;

    indicators::show_console_cursor(false);

    mWorker = std::thread([&mSpinner = this->mSpinner, &mRunning = this->mRunning]() {
        while (mRunning) {
            mSpinner->tick();
            if (mSpinner->current() == 100) {
                mSpinner->set_progress(0);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));

            std::cout.flush();
        }
    });
}

void Spinner::Stop(const std::string& message)
{
    if (!mRunning) {
        throw std::runtime_error("No spinner running!");
    }

    mRunning = false;

    mWorker.join();
    mSpinner->set_option(indicators::option::ShowSpinner { false });
    mSpinner->set_progress(100);
    mSpinner->tick();
    mSpinner->set_option(indicators::option::ForegroundColor { indicators::Color::green });
    mSpinner->set_option(indicators::option::PrefixText { "✔" });
    mSpinner->set_option(indicators::option::ShowSpinner { false });
    mSpinner->set_option(indicators::option::ShowPercentage { false });
    mSpinner->set_option(indicators::option::PostfixText { message });
    mSpinner->mark_as_completed();
    std::cout.flush();

    mSpinner.reset(nullptr);

    indicators::show_console_cursor(true);
}

ISpinner& ISpinner::Get()
{
    static Spinner spinner;
    return spinner;
}

} // namespace shirley::utils