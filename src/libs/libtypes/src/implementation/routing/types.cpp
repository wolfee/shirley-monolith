#include "../../interface/libtypes/routing/types.h"

#define _USE_MATH_DEFINES
#include <cmath>

namespace shirley::types::routing {

float GCoor::GetDistancePrecise(const GCoor& other) const
{
    const auto d1 = static_cast<double>(mLatitude) * (M_PI / 180.0);
    const auto num1 = static_cast<double>(mLongitude) * (M_PI / 180.0);
    const auto d2 = static_cast<double>(other.mLatitude) * (M_PI / 180.0);
    const auto num2 = static_cast<double>(other.mLongitude) * (M_PI / 180.0) - num1;
    const auto d3 = pow(sin((d2 - d1) / 2.0), 2.0) + cos(d1) * cos(d2) * pow(sin(num2 / 2.0), 2.0);

    return static_cast<float>(6376500.0 * (2.0 * atan2(sqrt(d3), sqrt(1.0 - d3))));
}

float GCoor::GetDistanceFast(const GCoor& other) const
{
    const float degLen = 110250.F;
    const auto x = mLatitude - other.mLatitude;
    const auto y = (mLongitude - other.mLongitude) * cosf(other.mLatitude);
    return degLen * sqrtf(x * x + y * y);
}

uint32_t Edge::GetFlags() const { return mFlags8mGeometryId24 >> 24; }

uint32_t Edge::GetGeometryId() const { return mFlags8mGeometryId24 & 0x00FFFFFF; }

} // namespace shirley::types::routing
