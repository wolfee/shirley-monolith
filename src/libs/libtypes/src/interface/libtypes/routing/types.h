#pragma once

#include <cinttypes>
#include <limits>

namespace shirley::types::routing {
struct GCoor {
    static constexpr float sInvalidCoor = -255.F;
    float mLongitude { sInvalidCoor };
    float mLatitude { sInvalidCoor };

    float GetDistancePrecise(const GCoor& other) const;
    float GetDistanceFast(const GCoor& other) const;
};

struct Junction {
    uint32_t mOutgoingCount8mOutgoingStart24;
    GCoor mGCoor;
};

struct Edge {
    enum Flags : uint32_t {
        FLAG_ONEWAY = 1U << 0U,
        FLAG_DIRECTION = 1U << 1U,
    };

    uint32_t mEndJunctionId;
    uint32_t mFlags8mGeometryId24;
    uint16_t mLength { 0 };
    uint16_t mTravelTime { std::numeric_limits<uint16_t>::max() };

    uint32_t GetFlags() const;
    uint32_t GetGeometryId() const;
};
} // namespace shirley::types::routing
