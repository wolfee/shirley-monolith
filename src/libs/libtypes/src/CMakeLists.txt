add_library(libtypes
  implementation/routing/types.cpp
)
target_link_libraries(libtypes
  PRIVATE project_options
  PRIVATE project_warnings
)
target_include_directories(libtypes INTERFACE interface)
