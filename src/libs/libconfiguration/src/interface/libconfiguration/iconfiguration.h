#pragma once

#include "types.h"
#include <boost/lexical_cast.hpp>
#include <exception>
#include <filesystem>
#include <memory>
#include <stdexcept>
#include <type_traits>

namespace shirley::configuration {

using namespace std::string_literals;

class IConfiguration {
public:
    class KeyNotFoundException : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };

    class InvalidBooleanException : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };

    virtual ~IConfiguration() = default;

    virtual void Parse(int argc, const char* const* argv) = 0;
    virtual void Parse(std::istream& stream) = 0;

    template <typename ConfigType>
    void Register(ConfigType config)
    {
        static_assert(std::is_base_of<ConfigValueTrait, ConfigType>(), "Getting configuration with invalid type!");

        Register(config.mKey, config.mDescription, std::is_base_of<ConfigValue<void>, ConfigType>());
    }

    template <typename ConfigType>
    typename ConfigType::underlying_type Get(ConfigType config) const
    {
        static_assert(std::is_base_of<ConfigValueTrait, ConfigType>(), "Getting configuration with invalid type!");
        static_assert(!std::is_base_of<ConfigValue<void>, ConfigType>(), "Getting configuration with void key!");

        std::string rawValue;
        try {
            rawValue = GetRawValue(config.mKey);
        } catch (const KeyNotFoundException& e) {
            return typename ConfigType::underlying_type(config.mDefault);
        }

        return CastToValue<typename ConfigType::underlying_type>(rawValue);
    }

    template <typename ConfigType>
    bool Has(ConfigType config) const
    {
        static_assert(std::is_base_of<ConfigValueTrait, ConfigType>(), "Getting configuration with invalid type!");

        return Has(config.mKey);
    }

    virtual std::string GetConfiguration() const = 0;

    static std::unique_ptr<IConfiguration> Create();

protected:
    virtual std::string GetRawValue(const Key& key) const = 0;
    virtual void Register(const Key& key, const char* const description, bool voidKey) = 0;
    virtual bool Has(const Key& key) const = 0;

private:
    template <typename T>
    T CastToValue(const std::string& value) const
    {
        return boost::lexical_cast<T>(value);
    }

    template <>
    bool CastToValue<bool>(const std::string& value) const
    {
        if (value == "true"s) {
            return true;
        } else if (value == "false"s) {
            return false;
        }
        throw InvalidBooleanException("Only true or false are accepted as boolean! Got " + value);
    }
};

} // namespace shirley::configuration
