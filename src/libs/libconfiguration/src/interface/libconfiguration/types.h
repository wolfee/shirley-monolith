#pragma once

#include <any>
#include <cstring>
#include <string>
#include <utility>

namespace shirley::configuration {

using namespace std::string_literals;

struct Key : public std::pair<const char* const, const char* const> {
    constexpr Key(const char* const tag, const char* const value)
        : std::pair<const char* const, const char* const>(tag, value)
    {
    }

    bool operator==(const Key& other) const { return strcmp(first, other.first) == 0 && strcmp(second, other.second) == 0; }

    std::string GetCombinedKey() const
    {
        if (strlen(first) != 0) {
            return first + "."s + second;
        }
        return second;
    }
};

struct ConfigValueTrait {
};

template <typename type>
struct ConfigValue : ConfigValueTrait {
    using underlying_type = type;

    constexpr ConfigValue(const char* const tag, const char* const value, type defaultValue, const char* const description = "")
        : mKey { tag, value }
        , mDefault { defaultValue }
        , mDescription { description }
    {
    }

    Key mKey;
    type mDefault;
    const char* const mDescription;
};

template <>
struct ConfigValue<std::string> : ConfigValueTrait {
    using underlying_type = std::string;

    constexpr ConfigValue(
        const char* const tag, const char* const value, const char* const defaultValue, const char* const description = "")
        : mKey { tag, value }
        , mDefault { defaultValue }
        , mDescription { description }
    {
    }

    Key mKey;
    const char* const mDefault;
    const char* const mDescription;
};

template <>
struct ConfigValue<void> : ConfigValueTrait {
    using underlying_type = void;

    constexpr ConfigValue(const char* const tag, const char* const value, const char* const description = "")
        : mKey { tag, value }
        , mDefault { nullptr }
        , mDescription { description }
    {
    }

    Key mKey;
    void* mDefault;
    const char* const mDescription;
};

} // namespace shirley::configuration