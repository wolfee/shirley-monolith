#pragma once

#include <boost/program_options.hpp>

#include "../interface/libconfiguration/iconfiguration.h"

namespace shirley::configuration {

class Configuration : public IConfiguration {
    friend class ConfigurationTestProxy;

public:
    void Parse(int argc, const char* const* argv) override;
    void Parse(std::istream& stream) override;
    std::string GetConfiguration() const override;

protected:
    std::string GetRawValue(const Key& key) const override;
    void Register(const Key& key, const char* const description, bool voidKey) override;
    bool Has(const Key& key) const override;

private:
    boost::program_options::variables_map mVariablesMap;
    boost::program_options::options_description mDescriptor { "Options" };
};

} // namespace shirley::configuration
