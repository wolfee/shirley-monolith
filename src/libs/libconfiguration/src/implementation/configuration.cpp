#include "configuration.h"
#include <boost/program_options/parsers.hpp>
#include <memory>
#include <sstream>

namespace shirley::configuration {

void Configuration::Parse(int argc, const char* const* argv)
{
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, mDescriptor), mVariablesMap);
    boost::program_options::notify(mVariablesMap);
}

void Configuration::Parse(std::istream& stream)
{
    boost::program_options::store(boost::program_options::parse_config_file(stream, mDescriptor), mVariablesMap);
    boost::program_options::notify(mVariablesMap);
}

std::string Configuration::GetConfiguration() const
{
    std::stringstream retVal;
    retVal << mDescriptor;
    return retVal.str();
}

std::string Configuration::GetRawValue(const Key& key) const
{
    const auto combinedKey = key.GetCombinedKey();

    if (mVariablesMap.count(combinedKey) == 0) {
        throw KeyNotFoundException("Key not found: " + combinedKey);
    }

    return mVariablesMap[combinedKey].as<std::string>();
}

void Configuration::Register(const Key& key, const char* const description, bool voidKey)
{
    if (voidKey) {
        mDescriptor.add_options()(key.GetCombinedKey().c_str(), description);
    } else {
        mDescriptor.add_options()(key.GetCombinedKey().c_str(), boost::program_options::value<std::string>(), description);
    }
}

bool Configuration::Has(const Key& key) const { return mVariablesMap.count(key.GetCombinedKey()) > 0; }

std::unique_ptr<IConfiguration> IConfiguration::Create() { return std::make_unique<Configuration>(); }

} // namespace shirley::configuration