#include <any>
#include <boost/program_options/errors.hpp>
#include <gtest/gtest.h>

#include "../implementation/configuration.h"

using namespace std::string_literals;

namespace shirley::configuration {
class ConfigurationTestProxy {
public:
    static void Register(
        shirley::configuration::Configuration& config, const shirley::configuration::Key& key, const char* const description)
    {
        config.Register(key, description, false);
    }

    static std::string GetRawValue(shirley::configuration::Configuration& config, const shirley::configuration::Key& key)
    {
        return config.GetRawValue(key);
    }

    static bool Has(shirley::configuration::Configuration& config, const shirley::configuration::Key& key) { return config.Has(key); }
};
} // namespace shirley::configuration

class ConfigurationTest : public testing::Test {
public:
    static constexpr int argc = 4;
    static constexpr const char* argv[argc] = { "program_name", "--rainbow=1", "--dash=2", "--the.cake=is_a_lie" };

    std::stringstream file { R"(
rainbow=2

[the]
cake=still_a_lie

[testing]
string=if it works
)" };
};

namespace config {
using shirley::configuration::ConfigValue;

constexpr auto rainbow = ConfigValue<int>("", "rainbow", 5);
constexpr auto dash = ConfigValue<int>("", "dash", 10);
constexpr auto rainbow_dash = ConfigValue<int>("", "rainbow_dash", 42);

namespace the {
    constexpr auto cake = ConfigValue<std::string>("the", "cake", "should_be_a_lie");
}

namespace testing {
    constexpr auto string = ConfigValue<std::string>("testing", "string", "");
}
} // namespace config

TEST_F(ConfigurationTest, ParsingConsoleWithoutRegister)
{
    shirley::configuration::Configuration config;
    EXPECT_THROW(config.Parse(argc, argv), boost::program_options::unknown_option);
}

TEST_F(ConfigurationTest, ParsingStreamWithoutRegister)
{
    shirley::configuration::Configuration config;
    EXPECT_THROW(config.Parse(file), boost::program_options::unknown_option);
}

TEST_F(ConfigurationTest, DefaultValues)
{
    using proxy = shirley::configuration::ConfigurationTestProxy;
    shirley::configuration::Configuration config;
    proxy::Register(config, config::rainbow.mKey, "");
    proxy::Register(config, config::dash.mKey, "");
    proxy::Register(config, config::the::cake.mKey, "");
    static constexpr int l_argc = 1;
    static constexpr const char* l_argv[argc] = { "program_name" };
    EXPECT_NO_THROW(config.Parse(l_argc, l_argv));
    EXPECT_EQ(config.Get(config::rainbow), 5);
    EXPECT_EQ(config.Get(config::dash), 10);
    EXPECT_EQ(config.Get(config::the::cake), "should_be_a_lie"s);
    EXPECT_FALSE(proxy::Has(config, config::the::cake.mKey));
}

TEST_F(ConfigurationTest, ParsingConsole)
{
    using proxy = shirley::configuration::ConfigurationTestProxy;
    shirley::configuration::Configuration config;
    proxy::Register(config, config::rainbow.mKey, "");
    proxy::Register(config, config::dash.mKey, "");
    proxy::Register(config, config::the::cake.mKey, "");
    EXPECT_NO_THROW(config.Parse(argc, argv));
    EXPECT_EQ(proxy::GetRawValue(config, config::rainbow.mKey), "1"s);
    EXPECT_EQ(proxy::GetRawValue(config, config::dash.mKey), "2"s);
    EXPECT_EQ(proxy::GetRawValue(config, config::the::cake.mKey), "is_a_lie"s);
    EXPECT_TRUE(proxy::Has(config, config::the::cake.mKey));

    EXPECT_FALSE(proxy::Has(config, config::rainbow_dash.mKey));
    EXPECT_THROW(proxy::GetRawValue(config, config::rainbow_dash.mKey), shirley::configuration::IConfiguration::KeyNotFoundException);
}

TEST_F(ConfigurationTest, ParsingStream)
{
    using proxy = shirley::configuration::ConfigurationTestProxy;
    shirley::configuration::Configuration config;
    proxy::Register(config, config::rainbow.mKey, "");
    proxy::Register(config, config::the::cake.mKey, "");
    proxy::Register(config, config::testing::string.mKey, "");
    EXPECT_NO_THROW(config.Parse(file));
    EXPECT_EQ(proxy::GetRawValue(config, config::rainbow.mKey), "2"s);
    EXPECT_EQ(proxy::GetRawValue(config, config::the::cake.mKey), "still_a_lie"s);
    EXPECT_EQ(proxy::GetRawValue(config, config::testing::string.mKey), "if it works"s);
    EXPECT_TRUE(proxy::Has(config, config::the::cake.mKey));

    EXPECT_FALSE(proxy::Has(config, config::rainbow_dash.mKey));
    EXPECT_THROW(proxy::GetRawValue(config, config::rainbow_dash.mKey), shirley::configuration::IConfiguration::KeyNotFoundException);
}

TEST_F(ConfigurationTest, ConsoleSupressesFile)
{
    using proxy = shirley::configuration::ConfigurationTestProxy;
    shirley::configuration::Configuration config;
    proxy::Register(config, config::rainbow.mKey, "");
    proxy::Register(config, config::dash.mKey, "");
    proxy::Register(config, config::the::cake.mKey, "");
    proxy::Register(config, config::testing::string.mKey, "");
    EXPECT_NO_THROW(config.Parse(argc, argv));
    EXPECT_NO_THROW(config.Parse(file));
    EXPECT_EQ(proxy::GetRawValue(config, config::rainbow.mKey), "1"s);
    EXPECT_EQ(proxy::GetRawValue(config, config::dash.mKey), "2"s);
    EXPECT_EQ(proxy::GetRawValue(config, config::the::cake.mKey), "is_a_lie"s);
    EXPECT_EQ(proxy::GetRawValue(config, config::testing::string.mKey), "if it works"s);
    EXPECT_TRUE(proxy::Has(config, config::the::cake.mKey));

    EXPECT_FALSE(proxy::Has(config, config::rainbow_dash.mKey));
    EXPECT_THROW(proxy::GetRawValue(config, config::rainbow_dash.mKey), shirley::configuration::IConfiguration::KeyNotFoundException);
}
