#include <gtest/gtest.h>

#include <libconfiguration/types.h>

#include <string>
#include <type_traits>

using namespace std::string_literals;

class ConfigurationTypesTest : public testing::Test {
};

TEST_F(ConfigurationTypesTest, Key)
{
    const shirley::configuration::Key key("rainbow", "dash");
    EXPECT_STREQ(key.first, "rainbow");
    EXPECT_STREQ(key.second, "dash");
    EXPECT_EQ(key.GetCombinedKey(), "rainbow.dash"s);

    const shirley::configuration::Key key2("", "dash");
    EXPECT_EQ(key2.GetCombinedKey(), "dash"s);
}

TEST_F(ConfigurationTypesTest, GenericConfigValue)
{
    constexpr auto config1 = shirley::configuration::ConfigValue<int>("tag", "key", 2);

    EXPECT_EQ(config1.mKey.GetCombinedKey(), "tag.key");
    EXPECT_EQ(config1.mDefault, 2);
    EXPECT_STREQ(config1.mDescription, "");

    constexpr auto config2 = shirley::configuration::ConfigValue<int>("tag", "key", 2, "description");
    EXPECT_STREQ(config2.mDescription, "description");
}

TEST_F(ConfigurationTypesTest, StringConfigValue)
{
    constexpr auto config1 = shirley::configuration::ConfigValue<std::string>("tag", "key", "default");

    EXPECT_EQ(config1.mKey.GetCombinedKey(), "tag.key");
    EXPECT_STREQ(config1.mDefault, "default");
    EXPECT_STREQ(config1.mDescription, "");

    constexpr auto config2 = shirley::configuration::ConfigValue<std::string>("tag", "key", "default", "description");
    EXPECT_STREQ(config2.mDescription, "description");
}
