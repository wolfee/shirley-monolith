#include <any>
#include <boost/program_options/errors.hpp>
#include <gtest/gtest.h>

#include <libconfiguration/iconfiguration.h>
#include <stdexcept>

using namespace std::string_literals;

class IConfigurationTest : public testing::Test {
public:
    void SetUp() override { config = shirley::configuration::IConfiguration::Create(); }

    static constexpr int argc = 5;
    static constexpr const char* argv[argc] = { "program_name", "--help", "--rainbow=1", "--dash=2", "--the.cake=is_a_lie" };

    std::stringstream file { R"(
rainbow=2

good_bool=true
bad_bool=0

[the]
cake=still_a_lie

[testing]
string=if it works
)" };

    std::unique_ptr<shirley::configuration::IConfiguration> config;
};

namespace config {
using shirley::configuration::ConfigValue;

constexpr auto rainbow = ConfigValue<int>("", "rainbow", 5);
constexpr auto dash = ConfigValue<int>("", "dash", 10);
constexpr auto rainbow_dash = ConfigValue<int>("", "rainbow_dash", 42);

constexpr auto good_bool = ConfigValue<bool>("", "good_bool", true);
constexpr auto bad_bool = ConfigValue<bool>("", "bad_bool", false);

constexpr auto help = ConfigValue<void>("", "help");

namespace the {
    constexpr auto cake = ConfigValue<std::string>("the", "cake", "should_be_a_lie");
}

namespace testing {
    constexpr auto string = ConfigValue<std::string>("testing", "string", "");
}
} // namespace config

TEST_F(IConfigurationTest, ParsingConsole)
{
    config->Register(config::help);
    config->Register(config::rainbow);
    config->Register(config::dash);
    config->Register(config::the::cake);

    config->Parse(argc, argv);

    EXPECT_EQ(config->Get(config::rainbow), 1);
    EXPECT_EQ(config->Get(config::dash), 2);
    EXPECT_EQ(config->Get(config::the::cake), "is_a_lie"s);

    EXPECT_EQ(config->Get(config::rainbow_dash), 42);
}

TEST_F(IConfigurationTest, ParsingStream)
{
    config->Register(config::rainbow);
    config->Register(config::good_bool);
    config->Register(config::bad_bool);
    config->Register(config::the::cake);
    config->Register(config::testing::string);

    config->Parse(file);

    EXPECT_EQ(config->Get(config::rainbow), 2);
    EXPECT_EQ(config->Get(config::the::cake), "still_a_lie"s);

    EXPECT_EQ(config->Get(config::testing::string), "if it works"s);
}

TEST_F(IConfigurationTest, Boolean)
{
    config->Register(config::rainbow);
    config->Register(config::good_bool);
    config->Register(config::bad_bool);
    config->Register(config::the::cake);
    config->Register(config::testing::string);

    config->Parse(file);

    EXPECT_TRUE(config->Get(config::good_bool));
    EXPECT_THROW(config->Get(config::bad_bool), shirley::configuration::IConfiguration::InvalidBooleanException);
}

TEST_F(IConfigurationTest, HasKey)
{
    config->Register(config::help);
    config->Register(config::rainbow);
    config->Register(config::dash);
    config->Register(config::the::cake);

    config->Parse(argc, argv);

    EXPECT_TRUE(config->Has(config::help));
    EXPECT_FALSE(config->Has(config::good_bool));
}