#include <libconverter/osm_reader.h>

#include <libconfiguration/iconfiguration.h>

#include <fstream>
#include <iostream>

namespace configuration {
using shirley::configuration::ConfigValue;

constexpr auto help = ConfigValue<void>("", "help", "Help screen");
constexpr auto config_file = ConfigValue<std::string>("", "config_file", "converter.ini", "Config file");
constexpr auto input = ConfigValue<std::string>("", "input", "", "Input map");
constexpr auto output = ConfigValue<std::string>("", "output", "", "Output file prefix");
} // namespace configuration

int main(int argc, char** argv)
{
    const auto config = shirley::configuration::IConfiguration::Create();
    config->Register(configuration::help);
    config->Register(configuration::config_file);
    config->Register(configuration::input);
    config->Register(configuration::output);
    config->Parse(argc, argv);

    if (config->Has(configuration::help)) {
        std::cout << config->GetConfiguration() << std::endl;
        return 0;
    }

    std::ifstream cfg { config->Get(configuration::config_file) };
    if (cfg.is_open()) {
        config->Parse(cfg);
    }

    shirley::converter::OSMReader reader;
    try {
        reader.Read(config->Get(configuration::input));
        reader.Write(config->Get(configuration::output));
    } catch (const std::exception& e) {
        std::cout << e.what() << std::endl;
        return -1;
    }
    return 0;
}
