#! /usr/bin/env node

require('dotenv').config();

const rpc = require('json-rpc2');

const port = process.env.PORT || 8080;

const routePlanner = rpc.Client.$create(process.env.ROUTE_PLANNER_PORT || 55544, process.env.ROUTE_PLANNER_HOST || 'localhost');
const geometries = rpc.Client.$create(process.env.GEOMETRIES_PORT || 22222, process.env.GEOMETRIES_HOST || 'localhost');
const closestlink = rpc.Client.$create(process.env.CLOSEST_LINK_PORT || 11666, process.env.CLOSEST_LINK_HOST || 'localhost');

const server = rpc.Server.$create({
    'websocket': false,
    'headers': {
        'Access-Control-Allow-Origin': '*'
    }
});

server.expose('plan_route', (args, opt, callback) => {
    console.log(`plan_route - ${JSON.stringify(args)}`);

    closestlink.call('closest_link', { longitude: args.waypoints[0].longitude, latitude: args.waypoints[0].latitude }, (err, closestLinkFrom) => {
        if (err) {
            throw new Error(err);
        }

        closestlink.call('closest_link', { longitude: args.waypoints[1].longitude, latitude: args.waypoints[1].latitude }, (err, closestLinkTo) => {
            if (err) {
                throw new Error(err);
            }

            const routingParams = { "from": closestLinkFrom, "to": closestLinkTo };

            routePlanner.call('plan_route', routingParams, (err, routingResult) => {
                if (err) {
                    throw new Error(err);
                }

                const geometyIds = routingResult.map(element => element.geometryId);

                geometries.call('get_concatenated_polyline', geometyIds, (err, geometryResult) => {
                    if (err) {
                        throw new Error(err);
                    }

                    const result = {
                        distance: 0,
                        duration: 0,
                        polyline: geometryResult
                    };

                    for (const element of routingResult) {
                        result.distance += element.length;
                        result.duration += element.travelTime;
                    }

                    callback(null, result);
                });
            });
        });
    });

});

console.log("starting server...");
server.listen(port, '0.0.0.0');