#! /usr/bin/env node

require('dotenv').config();

const mongoose = require('mongoose');
const schema = require('./schema');
const rpc = require('json-rpc2');

const port = process.env.PORT || 11666;

main = async () => {
    await mongoose.connect(process.env.MONGO_URL);

    const server = rpc.Server.$create({
        'websocket': false,
        'headers': {
            'Access-Control-Allow-Origin': '*'
        }
    });

    server.expose('closest_link', async (args, opt, callback) => {
        console.log(`closest_link - ${JSON.stringify(args)}`);

        const retVal = await schema.findOne({
            geometry: {
                $nearSphere: {
                    $geometry: {
                        type: "Point",
                        coordinates: [
                            args.longitude,
                            args.latitude
                        ]
                    }
                }
            }
        });
        if (retVal.edges.length > 0) {
            callback(null, retVal.edges[0]);
            return;
        }

        throw new Error("Unable to find link!");
    });

    console.log("starting server...");
    server.listen(port, '0.0.0.0');
}

main();