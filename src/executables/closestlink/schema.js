const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        edges: [
            {
                type: Number,
                required: true
            }
        ],
        geometry: {
            type: {
                type: String,
                required: true
            },
            coordinates: [
                [
                    {
                        type: Number,
                        required: true
                    }
                ]
            ]
        }
    },
    { collection: process.env.MONGO_COLLECTION_NAME }
);

module.exports = mongoose.model('schema', schema);
