add_subdirectory(converter)

function(create_npm_programs target)
  file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${target})
  add_custom_target(${target} ALL
    COMMAND ${CMAKE_COMMAND} -E rm -f *.tgz
    COMMAND npm pack
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${target}
  )
  install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${target}/ DESTINATION ${CMAKE_SOURCE_DIR}/target FILES_MATCHING PATTERN "*.tgz")
endfunction()

create_npm_programs(closestlink)
create_npm_programs(closestlinkuploader)
create_npm_programs(geometries)
create_npm_programs(routeplanner)
create_npm_programs(routing_server)
