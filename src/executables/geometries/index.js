#! /usr/bin/env node

require('dotenv').config();

const rpc = require('json-rpc2');
const libgeometries = require('@shirley/libgeometries');

const data = new libgeometries.GeometriesData();
data.load(process.env.PORT || "/content");

const geometries = new libgeometries.Geometries(data);
delete data;

const port = process.env.PORT || 22222;

const server = rpc.Server.$create({
    'websocket': false,
    'headers': {
        'Access-Control-Allow-Origin': '*'
    }
});

server.expose('get_coors', (args, opt, callback) => {
    console.log(`get_coors - ${JSON.stringify(args)}`);
    callback(null, geometries.getCoors(args));
});

server.expose('get_polylines', (args, opt, callback) => {
    console.log(`get_polylines - ${JSON.stringify(args)}`);
    callback(null, geometries.getPolylines(args));
});

server.expose('get_concatenated_polyline', (args, opt, callback) => {
    console.log(`get_concatenated_polyline - ${JSON.stringify(args)}`);
    callback(null, geometries.getConcatenatedPolyline(args));
});

console.log("starting server...");
server.listen(port, '0.0.0.0');