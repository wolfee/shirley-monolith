#! /usr/bin/env node

require('dotenv').config();

const mongoose = require('mongoose');
const schema = require('./schema');
const libdata = require('@shirley/libclosestlinkdata');

class BarProgress {
    start = (maxValue, displayStep, _break, lineBreak, message) => {
        this.mMaxValue = maxValue;
        this.mDisplayStep = displayStep;
        this.mBreak = _break;
        this.mLineBreak = lineBreak;
        this.mValue = 0;
        process.stdout.write(message);
    }

    step = () => {
        if (this.mValue % this.mLineBreak == 0) {
            process.stdout.write(`\n${this.mValue.toString().padStart(this.mMaxValue.toString().length, ' ')} `);
        }

        this.mValue++;

        if (this.mValue % this.mDisplayStep == 0) {
            process.stdout.write('.');
        }
        if (this.mValue % this.mBreak == 0) {
            process.stdout.write(' ');
        }
        if (this.mValue % this.mLineBreak == 0) {
            process.stdout.write(`${(this.mValue / this.mMaxValue * 100.0).toFixed(0)}%`);
        }
    }

    stop = () => {
        if (this.mValue % this.mLineBreak != 0) {
            while (this.mValue % this.mLineBreak != 0) {
                this.mValue++;
                if (this.mValue % this.mDisplayStep == 0) {
                    process.stdout.write(' ');
                }
                if (this.mValue % this.mBreak == 0) {
                    process.stdout.write(' ');
                }
            }
        }
        process.stdout.write('100%\n');
        console.log("✔ Done");
    }
};

main = async () => {
    await mongoose.connect(process.env.MONGO_URL);

    const data = new libdata.ClosestLinkData();
    data.load(process.env.CLU_INPUT || ".");

    await schema.collection.dropIndexes();
    await schema.collection.drop();

    const dataBag = [];

    const progress = new BarProgress();
    progress.start(data.getCount(), 1000, 10000, 50000, "Upload closest link to database");

    for (i = 0; i < data.getCount(); ++i) {
        const link = data.get(i);
        if (!link.valid) {
            continue;
        }
        const geometry = [];
        for (const g of link.gcoors) {
            const gcoors = [];
            gcoors.push(g.longitude);
            gcoors.push(g.latitude);
            geometry.push(gcoors);
        }
        const d = new schema({
            edges: link.ids,
            geometry: {
                type: "LineString",
                coordinates: geometry
            }
        });
        progress.step();
        dataBag.push(d);
        if (dataBag.length === 1000) {
            await schema.collection.insertMany(dataBag);
            dataBag.length = 0;
        }
    }

    if (dataBag.length > 0) {
        await schema.collection.insertMany(dataBag);
    }
    progress.stop();

    console.log("Indexing database");
    await schema.collection.createIndex({ geometry: "2dsphere" });
    console.log("✔ Done");

    process.exit(0);
}

main();