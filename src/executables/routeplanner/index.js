#! /usr/bin/env node

require('dotenv').config();

const rpc = require('json-rpc2');
const routing = require('@shirley/librouting');

const data = new routing.RoutePlannerData();
data.load(process.env.PORT || "/content");

const planner = new routing.RoutePlanner(data);
delete data;

const port = process.env.PORT || 55544;

const server = rpc.Server.$create({
    'websocket': false,
    'headers': {
        'Access-Control-Allow-Origin': '*'
    }
});

server.expose('plan_route', (args, opt, callback) => {
    console.log(`plan_route - ${JSON.stringify(args)}`);
    callback(null, planner.planRoute(args));
});

console.log("starting server...");
server.listen(port, '0.0.0.0');