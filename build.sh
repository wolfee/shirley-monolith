#!/bin/bash

cd /src
cmake -B build . -DCMAKE_BUILD_TYPE=Release
cmake --build build --target install

npm-cli-adduser -u wolfee -p password -e wolfee.dm@gmail.com -r $NPM_REGISTRY
npm config set @shirley:registry $NPM_REGISTRY

sh /src/upload_npm.sh $NPM_REGISTRY
