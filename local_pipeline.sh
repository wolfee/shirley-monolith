#!/bin/bash

rm -rf docker/build_result
docker kill build_verdaccio
docker run --rm --name build_verdaccio -d -p 64000:4873 verdaccio/verdaccio

NPM_REGISTRY=http://127.0.0.1:64000/

mkdir docker/build_result
docker run --rm --name build -it -v conan:/root/.conan:Z -v ${PWD}:/src -v ${PWD}/docker/build_result:/src/target --network="host" --env NPM_REGISTRY=$NPM_REGISTRY --env LOCAL_BUILD=TRUE wolfee/clang-build:latest /src/build.sh

docker build -t converter -f docker/ConverterImage --network="host" --build-arg NPM_REGISTRY=$NPM_REGISTRY --no-cache docker/

docker build -t closestlink -f docker/ClosestlinkImage --network="host" --build-arg NPM_REGISTRY=$NPM_REGISTRY --no-cache docker/
docker build -t geometries -f docker/GeometriesImage --network="host" --build-arg NPM_REGISTRY=$NPM_REGISTRY --no-cache docker/
docker build -t routeplanner -f docker/RouteplannerImage --network="host" --build-arg NPM_REGISTRY=$NPM_REGISTRY --no-cache docker/
docker build -t routingserver -f docker/RoutingserverImage --network="host" --build-arg NPM_REGISTRY=$NPM_REGISTRY --no-cache docker/

rm -rf docker/build_result
docker kill build_verdaccio