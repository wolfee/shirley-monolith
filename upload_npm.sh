#!/bin/bash

for FILE in ./target/*.tgz; do
        tar -xzf $FILE;
        PACKAGE=`jq -M -r .name package/package.json`;
        VERSION=`jq -M -r .version package/package.json`;
        rm -rf package;
        echo "unpublish $PACKAGE@$VERSION from $NPM_REGISTRY"
        if [ $LOCAL_BUILD ]
        then
                npm unpublish --force --registry $NPM_REGISTRY $PACKAGE@$VERSION;
        else
                DELETE_ENDPOINT=`curl -s --header "PRIVATE-TOKEN: $API_TOKEN" "$PACKAGE_REGISTRY" | jq --arg PACKAGE "$PACKAGE" --arg VERSION "$VERSION" -r '.[] | select( .name == $PACKAGE and .version == $VERSION) | ._links.delete_api_path'`
                curl --request DELETE --header "PRIVATE-TOKEN: $API_TOKEN" "$DELETE_ENDPOINT"
        fi
        echo "publish $FILE"
        npm publish $FILE;
done
